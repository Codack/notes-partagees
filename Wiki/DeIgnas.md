![img](https://f7ea542666.cbaul-cdnwnd.com/4f2a2f73bfe079b49026d60c677997b2/200000041-981d1981d3/Embl%C3%A8mes-08.png?ph=f7ea542666)

Emblème des De Ignas

**Emblème des De Ignas** : Un grand voilier entouré de deux tours, sous le blason se trouve un laurier symbole d'alliance à la famille royale.

**• Tomar De Ignas :** (H) Humain, 50aine, Dirigeant de l'entreprise familiale, Allié des [Siannodel](.\Wiki\Siannodel.md), Membre du Voile d'Obsidienne

**• Amane De Ignas :** (F) Humaine, 50aine, Femme de Tomar, Originaire de l'Empire Tudo

**• Isou :** (F) Humaine, 50aine, Soeur de Amane, Originaire de l'Empire Tudo

**• Yakabane :** (H) Humain, 50aine, Mari de Isou, Originaire de l'Empire Tudo, Lanceur de sort, associé de Tomar.

**• Oskar De Ignas :** (H) Humain, 30aine, Fils aîné de Tomar et Amane

**• Lidia De Ignas :** (F) Humaine, 30aine, Fille de Tomar et Amane

**• Otan De Ignas :** (H) Humain, 30aine, Fils de Tomar et Amane

**• Inala De Ignas :** (F) Humaine 25aine, Fille de Tomar et Amane

**• Hector :** (H) Humain, 50aine, Majordome de la famille
