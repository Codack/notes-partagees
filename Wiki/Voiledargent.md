La ville se situe au sud d'Ombrelune fait partie des villes du Vieil Empire, qui existait avant l'extension de ce dernier.

**LES TROIS ENCLUMES**

**• La Forgerie .** Forge - Taverne

*Durim Bronzemain, Hurgrim Bronzemain (frère) et leur cousin Lobrim Pattedefer (possède un jambe en fer), Nain, Barbe rousse longue et tressé avec beaucoup de bijoux*

*Cuvée Pattefer : Bière naine brasser par les nains de la Forgerie*

*Armure garantie à vie*

**•** **Les Potions de Jasmin .** Apothicaire

**•** **La Taverne du Bourrin .** Auberge-Taverne

*Magnoc, Demi-orc (50aine) (F) , Bien bâti, Tatouage partout y compris sur le visage.*

**•** **Les Trucs et le Vrac .** Echoppe générale

**•** **Place de la Forge**

**•** **La Cape Volage .** Tailleur

**QUARTIER DE L'AUBE**

**•** **Le Hall Justice de Voildargent .** Hall de justice

**•** **L'Aile de l'Aurore .** Restaurant

**•** **Aux Portes de la Pensé .** Librairie

**•** **Les Archives de Voildargent .** Archives

**•** **L'Étoile du Nord .** Hospice et Hôpital

**•** **Le Tintement .** Bijouterie

**• Autel du Forgeur d'Âme**

**L'ESPLANADE**

**•** **La Terrasse .** Restaurant

**•** **Ernesto Panaderia .** Boulangerie

**•** **Le Temple de Platine** . Temple de Bahamut

**QUARTIER DU REMPART**

**•** **Le Bouclier du Colosse .** Auberge-Taverne

*Ulys Rougecape . Halfelin (40aine) (H) Cheveux long attaché en bordel au dessus de sa tête, petite barbiche en bataille.*

**•** **Mixtures et Machins .** Echoppe générale

**•** **Le Fort du Savoir .** Temple d'Erathis

*Bâtiment imposant à l'aspect de château fort.*

*Nera Calangre . Haute Clerc, Femme (40 aine) . Cheveux court, brun. Elle porte une armure cérémonielle ainsi qu'une masse d'arme à l'image du symbole d'Erathis (Roue crantée). Elle porte une tunique par-dessus l'armure qui tombe en cascade. Les tissus arrivent mi-cuisse. Autour du cou, elle porte le symbole de la déesse et sur la toge est brodé, en fil blanc, le même symbole. Elle est droite, pragmatique.*

*Lucina Hornebrun, petite-fille du doyen de la famille*

*Lubron Hornebrun, Doyen de la communauté*

**•** **La Librairie du Pont des Poèmes .** Librairie

*Ander Joielux, Halfelin (40aine) (H), Cernes sous les yeux. Cheveux roux en bataille, barbiche, et rouflaquettes. Il porte une tunique courte de couleur rougeâtre.*

**•** **La Réserve de l'Est .** Caserne
