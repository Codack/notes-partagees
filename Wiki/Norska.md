# Norska

Pays très froid et très inhospitalier

Peuplé de tribus barbares

## Vallée des Echos

Vallée qui permet d'entré en Norska (entre Givrefort et Norska en gros)

## L'étoile du nord

Dernier avant poste des garnison de Givrefort

## Faille des ames

Faille profonde dans le sol ou errent des ames. Les âmes de la faille attaquent quiconque fait trop de bruit.

## Roi Kilgar

Roi des nains de Norska

## Main de dieu

Main gigantesque plantée dans la montagne. Elle appartenait à un golem de destruction auparavant.

![image-20231206211338869](C:\Users\theot\AppData\Roaming\Typora\typora-user-images\image-20231206211338869.png)

## Centre archéologique

Gigantesque golem de destruction enterré dans la montagne. 

![image-20231206211603201](C:\Users\theot\AppData\Roaming\Typora\typora-user-images\image-20231206211603201.png)
