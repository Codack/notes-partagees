# Fort Ecarlate

Garnison mineure au sud-est. Elle est orientée pour parer à d'éventuelles attaques maritimes venant de la côte est.

## LA PLACE DE LA SENTINELLE

*Quartier fréquenté par beaucoup de soldats et de gardes*

**• École de l'Ecu Ecarlate** . Ecole Militaire

**•** **Le Pavois Troué** . Taverne (habitué des militaires, notamment des gradés : officiers et capitaine)

*Midon Volfrange, Aaracokra, tavernier*

**•** **Hall de Justice de Fort d'Écarlate .** Hall de Justice

*Voltura Ambrecoeur, Demi-elfe, vieille, Porte-justice**

## TRAVELION

*Quartier artisans*

**•** **Forgelion** . Forge

*Duncan, Homme*

**•** **Famille Du Bois** . Menuiserie

**•** **La Caserne de l'Ouest .** Caserne

**•** **Tissevoile** - Tisserand

**•** **Autel d'Erathis**

*Statue d'une jeune femme, portant à bout de bras une roue cranté, elle est en armure et porte un livre dans son autre main.*

**•** **Autel de Moradin**

*Enclume dans une alcove arborant le symbole de Moradin*

## UPPER CORNER

*Quartier résidentiel*

**•** **Renard Masqué .** Taverne

**•** **Chez Urchak .** Apothicaire

**•** **Chez Maggie, Libraire .** Librairie

## PORT ÉCARLATE

**•** **La Caserne de l'Est .** Caserne

**•** **Le Solarium .** Temple de Pelor

*Orcane Lijane, Elfe, prêtre de Pelor*

**•** **L'Illuminée** . Taverne

**•** **Le Phare**
