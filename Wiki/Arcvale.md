Arcvale se trouve au sud d'Ombrelune. C'est une ville de passage et un havre fertile pour les commerçants de l'Empire et des environs. La cité est connue pour ses denrées rares à des prix imbattables et pour ses nombreux établissements de loisirs.

**LA BARGE**

*Quartier plutôt pauvre de la ville. Il est situé de l'autre côté du fleuve qui traverse la ville.*

**• Docks**

**•** **Chez Rustine** . Poissonerie

*Rustine . Gobelin (H)*

**•** **Musham & Borak** . Echoppe

**•** **Comptoir Dramacnaien**

**•** **Comptoir Ausra**

**•** **Autel d'Erathis**

**•** **La Pointe de Fer** **.** Forge, Propriété [Titarion](.\Wiki\Titarion.md) 

**•** **Entrepôt des Titarions**

**•** **Chez Forjette .** Bordel - Salle de jeux

**LA POINTE DE L'EST**

*Quartier politique et aristocratique***
**

**•** **Le Hall de Justice de Arcvale .** Hall de justice

**•** **La Soufflante .** Caserne

**•** **Sumar, le Bon Endroit.** Echoppe

**•** **Hôtel de Ville .** Archive et Hôtel de Ville

**•** **Le Pain Gourmand .** Boulangerie-Pâtisserie

**•** **Académie des Pourparlers .** Académie de politique, art de la guerre, stratégie, marchandage, finance

**•** **Bibliothèque d'Arcvale**

**LA PORTE DU SUD**

**•** **La Brillante .** Caserne

**•** **Mme Tumnas** . Echoppe

**•** **Potions en tout genre (Apothicaire)**

**•** **Le Bouillon .** Taverne-Auberge-Bordel Pauvre

**•** **La Cinglante .** Forge, Propriété [Titarion](.\Wiki\Titarion.md) 

*Margarete Fibrechanvre . Demi-Elfe (F)*

**•** **Le Grand Marteau .** Temple de Moradin

**LA PLACE BORÉAL**

*Place marchande*

**•** **Xio khan Gizo .** Forge

*Xio khan . Humain (H) (40aine)* 

**•** **Le Miroir Grossissant .** Boutique de Magie (Fermé)

*Kirina [Luvarne](.\Wiki\PNJ.md) . Demi-elfe (F) (jeune)* 

**•** **[Titarion](.\Wiki\Titarion.md), Travail du bois .** Menuiserie Imposante, Propriété Titarion

*Alonzo Brigande . Humain (H)* 

**•** **Le Repaire de l'aventurier .** Taverne-Auberge Imposante Modeste

*Konrad. Minotaure (H) (50aine)*

**QUARTIER** **DE L'ASTRE**

*Quartier résidentiel de la ville, habité par la classe moyenne de la ville : les artisans, les soldats*

**•** **La Page Blanche.** Libraire

**•** **Le Temple du Père de l'Aube .** Temple de Pelor

**•** **Le Chat Rugissant .** Petite taverne modeste , Propriété Titarion

*Janet . Humain (F) (40aine)*

**•** **Autel de Bahamut**

**•** **Maison des Ombres Éclatantes .** 14 rue de l'Astre

**• La Source .** Bains publics

• **La Caserne d'Ambre**

*Philip Oblem (H) Capitaine de la Garde d'Ambre*

**QUARTIER AURORE**

*Quartier plus riche et confortable***
**

**•** **L'Apostrophe .** Librairie

**•** **Cape et Chapeau de Romuald** . Tailleur

**•** **La Boulangerie de l'Aurore .** Boulangerie

**•** **L'Automnale .** Taverne riche **,** Propriété Titarion

**•** **Le Cathédrale de Platine .** Temple de Bahamut

**•** **La Tapisserie .** Salle de jeux chic - Maison de passe
