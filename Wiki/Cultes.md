# ERATHIS

## Podfick Grismenton
(H) Nain, vieux (Plus de 100 ans), habit simple de prêtre ermite avec la symbolique d'Erathis, barbe et cheveux gris, bâton.

Il surveille et entretien un cimetière de guerriers ancien aux abords de Pierrehavre, non-loin de Ambrecôte. Le cimetière fut protégé d'un golem de ronces et de morts-vivants par les Ombres Éclatantes.

## Néra Calangre
(F) Humaine, 40aine, Cheveux court, brun. Elle porte une armure cérémonielle ainsi qu'une masse d'arme à l'image du symbole d'Erathis (Roue crantée). Elle porte une tunique par-dessus l'armure qui tombe en cascade. Les tissus arrivent mi-cuisse. Autour du cou, elle porte le symbole de la déesse et sur la toge est brodé, en fil blanc, le même symbole. Haute Clerc du Fort du Savoir à Voildargent .

## Lubron Hornebrun
(H) Humain, 80aine, Dégarni, aveugle, cheveux blancs, barchiche chaotique, peau ridé. Il porte ses habits de cérémonie. 

C'est le doyen de la communauté d'Erathis, de Voildargent . Il est l'ancien Haut Clerc du Fort du Savoir.

## Lucinna Hornebrun
(F) Petite fille du Doyen, 10 ans. Cheveux long blond attachés en queue de cheval, habit simple, elle porte un capuchon bleu. Elle est très rebelle, et elle a comme objectif de devenir une aventurière. Ses parents sont décédé d'une attaque de brigand lorsqu'elle était petite et son grand-père est trop vieux pour s'occuper d'elle. C'est pourquoi est elle sous la tutelle de Néra au Fort du Savoir.

Elle est intriguée par Nawëlis, Fasciné par les armures de Caleb et Donaar.

# PELOR 

## Orcane Lijane
(H) Haut-elfe, âge inconnue, chauve, trait fin. Tenue de prière avec la symbolique de Pelor. 

Orcane est le prêtre en charge du Solarium, le temple de Pelor à Fort Écarlate.

## Ichmar Barbouc
(H) Inconnue, Haut-Clerc de Pelor à l'Aiguille du Soleil de [Port Daran](.\Wiki\PortDaran.md).

Il a été mentionné par Orcane comme une solution pour sauver l'âme de Galan..

# TARANIS

## Rabjack Silfelangue (Mort)
(H) Demi-elfe, 30aine, Paladin et Chef de Armure de la Foi, Cheveux court, châtain, athlétique. Il a les yeux en amande d'un bleu profond. Il porte une armure simple, ainsi qu'un symbole de Taranis autour du cou. Il à un accent espagnol. 

Rabjack a été pointé à Caleb par Taranis comme un ennemi et la source de son mal-être. Caleb et les Ombres Éclatantes ont suivi Rabjack sur l'île de Démarian et après l'avoir affronter Caleb lui a asséné un coup fatal.

# MORADIN  

## Prêtre Frédérik
(H) Nain, vieux, barbe épaisse blanche tressée, cheveux dégarni, habit de forgeron. C'est dans un de ses registres que Caleb a obtenu de nouvelles informations concernant le culte de Moradin

# KORD

## Tholas
(H) Humain, moine de Kord, 25aine. Il proposa à Caleb de se battre à mains nues lorsque se dernier c'est intéressé au pratique martiale des moines de Kord.

# BAHAMUT

## Abys
(H) Humain, jeune moine de Bahamut, il a aidé Donaar à renoué avec sa foi 
