![img](https://f7ea542666.cbaul-cdnwnd.com/4f2a2f73bfe079b49026d60c677997b2/200000020-00e0f00e11/emblem%20titarion-01.png?ph=f7ea542666)

**Emblème de Titarion** : Un oiseau sortant des flammes

**LA FAMILLE**

**• Aoth Titarion** : (H) Humain, père de Koxvar, 50 ans

• **Nephis Titarion (anciennement Vontrefor)** : (F) Humaine, mère de Koxvar, 45ans

• **Misa Titarion :** (F) Humaine, soeur de Koxvar, 22 ans . Jeune femme. Cheveux châtain clair court coupé en carré. Tenue de simple, corset de cuir et jupe courte descendante vers l'arrière. Pantalon de toile serré avec de multiples poches. Deux poches de ceinture, une grande à l'arrière et une petite devant. Une dague à la ceinture. Des bottes de voyageurs hautes en cuir de qualité. Une chemise en lin sobre et une veste en cuir ouverte . Un manteau en tissu épais et une écharpe.

• **Tavarn Titarion** : (H) Humain, Frère de Aoth, 53 ans, il est parti en Estalie suite à une dispute avec Aoth et Nephis. Il n'a pas donné de nouvelles depuis environ 30 ans

• **I****ka Macmornac (anciennement Titarion)** : (F) Humaine, soeur de Aoth, 47 ans, mariée à Arthy Macmornac. Elle habite Gardemont avec son marie.

• **Arthy Macmornac** : (H) Nain, 60 ans , alchimiste, il habite Gardemont avec Ika

• **Hector Titarion** : (H) Humain, 70 ans, Il habite l'Enclume mais ne donne pas de nouvelles

• **Théotime Vontrefor** : (H) Humain, 42 ans, Frère de Nephis, Il habite l'Enclave.

**EMPLOYÉS :**

• **Henry Auvrac** : (H) Humain , 38 ans , homme en chair habillé simplement avec une sacoche en bandoulière. Une barbe brune taillé proprement. Une cape de tissu épais un bonnet .Il est chauve.

• **Philip Oblem** : (H) Humain, 50aine. Capitaine des Gardes d'Ambres .Cheveux grisonnant dreadlocks épaisses et attachées, barbe et moustache propre, peau sombre. Il a porte une cuirasse avec le symbole des Titarion gravé et une épée longue à la ceinture.

**• La Garde d'Ambre** : Milice privée des Titarions, elle compte une trentaine de membres et elle assure la sécurité des Titarion et des Ombres Éclatantes dans la ville d'[Arcvale](.\Wiki\Arcvale.md).

• **Thom Borjac** : (H) Humain, 19 ans. Ancien acolyte de Fargas, il a été sauver par les Ombres Éclatantes et travail pour les Titarions à présent. Il s'occupe de la maison des Ombres Éclatantes à [Arcvale](.\Wiki\Arcvale.md).
