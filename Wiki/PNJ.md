# FORT ÉCARLATE

### Luis Zamarn
(H) Humain (35aine), Homme à la peau halé. Une moustache soigneusement taillé et des cheveux long attaché en un chignon négligé. Des habits fin, couleurs bleue qui rappel la mer. C'est un artiste qui a proposé au groupe de les tatouer avec des pierres précieuses et un enchantement afin d'améliorer leurs caractéristiques.

### Ébol Moudouc
(H) Halfelin, Cheveux court brun . Capitaine du Goéland, navire marchand rapporté par les Ombres après avoir été volé par Rabjack et son groupe. Au moment de rendre la bateau à Ébol, les Ombres ont passé un marché : 200 po par mois pendant 3 ans.

### Zeblis

Frere de Galan, tué par Galan. Il avait été envoyé par son père pour tromper la vigilance de Galan et le tuer.

# LA GIROUETTE

###  Balmur

(H) Nain, Capitaine de la Girouette, Barbe rousse coupé d'un côté, œil droit fou, doué avec une hache, habillé de manière très simple, il possède des tatouages sur tout le dos et qu'on commencent a voir sur le dos de ses bras et le haut de ses épaules.

Ils ont récemment été embauché par les [De Ignas](.\Wiki\DeIgnas.md).

###  Pico

(H) Halfelin, second de Balmur. La Girouette appartient à sa famille

**PIERREHAVRE :**

###  Kimbale Moudouc

(H) Halfelin, 60aine, Chef du village

**BLANCHECOLLINE :**

### Miria Lejuste

(F) Humaine, 50aine, Tenancière du relais de voyageur "Chez Mimi"

###  Joseph Lejuste

(H) Humain, 50aine, Tenancier du relais de voyageur "Chez Mimi"

###  Nawëlis Lejuste

(F) Humaine, Fille de Joseph et Miria, 20aine, Apprenti à l'Académie du Solstice à Griffinvale

###  Dovan Lejuste 

(H) Humain, Fils de Joseph et Miria, 22 ans, Soldat à Fort Écarlate 

Jeune homme d'1,85m, cheveux long et brun. Il possède une légère pilosité faciale, ainsi qu'une cicatrice au dessus du sourcil droit. . Il est vêtu d'une armure en cuir renforcé légèrement en métal.

###  Diana Le juste 

(F) Humaine, Fille de Joseph et Miria, 15 ans, Cherche à voyager à la capitale pour devenir chanteuse. Elle a rejoins un groupe de barde pour parcourir l'Empire et devenir chanteuse.

###  Tomen

(H) Humain, 40aine, Chef du village

# AQAR

### Silvar Blanchetempête

(H) Elfe Magicien. Allié de la Grande Archive de [Griffinvale](.\Wiki\Griffinvale.md) et de l'Ordre du Savoir. Il a scellé les Clés de Golems de Destruction pour les Ombres Éclatantes et à proposé à Saphir de prendre le cercle de téléportation de sa tour en note afin de revenir étudier les artéfacts.

### Braxxar Vitrine

(H) Demi-elfe, habits rapiécés et en mauvais état. Guide de la région, embauché par Trimar pour guider Nawëlis au cimetière de caravanes..

# [GRIFFINVALE](.\Wiki\Griffinvale.md)

### Norha ERETIL

(F) Elfe Mère de Saphir , Libraire à [Griffinvale](.\Wiki\Griffinvale.md) L'Étoile Polaire. Elle est douce, posée, chaleureuse. elle a soutient sa fille quoi qu'elle entreprenne.

### Belor ERETIL

(H) Elfe Père de Saphir , Libraire à [Griffinvale](.\Wiki\Griffinvale.md) (L'Étoile Polaire depuis 45 ans) - 120 ans il est généreux, exigeant, posé. Il est très impliqué dans le développement de sa fille et lui fournit toujours de la matière pour travailler.

### Nasras ERETIL

(H) Elfe Frère de Saphir , Marchand à Malkim (40aine d'années) . Saphir n'a connu Nasras qu'a travers les descriptions de ses parents. Gentil, serviable et généreux. Elle l'a rencontrer récemment durant son séjour à [Griffinvale](.\Wiki\Griffinvale.md).

### Bic

(F) Pixie ami de Podfick, Alcoolique.

### Grog Brisecrâne

(H) Goliath, Mercenaire, Chasseur de prime, rencontrer à Voildargent et revu à Arcvale

### Trimar Gornebor

(H) Demi-elfe, 40aine, chasseur de prime . Il a une pilosité faciale inégale. Habillé en armure de cuir clouté. A sa ceinture, il y a plusieurs dague ainsi que des fioles ésotériques. Drapé dans un longue et épaisse cape grise. Dans son dos on peut apercevoir une épée ainsi que le manche d'une arbalète. Il possède de long cheveux attachés et un chapeau de cocher est bien visé sur sa tête. Ses yeux sont perçant. Il a une voix grave et mâche ses mots.

### Fren Lowgrove

(H) Génasi de l'air, Ancien membre des Ombres Éclatantes partie pour rejoindre Ombrelune.

### Kirina Luvarne

(F) Demi-elfe, 30aine, Enchanteresse du Miroir Grossissant à [Arcvale](.\Wiki\Arcvale.md). Elle a été arrêté et accusé de vendre des objets magiques de manière illégale. Elle a été arrêté sur ordre de l'Assemblée du Cerbère. Elle prétend avoir été roulé par un gnome.

### Alina Von Shaften

(F) Baronne de Brandbourg une ville de l'Empire Taldorien. C'est une ami de Tomar [de Ignas](.\Wiki\DeIgnas.md).

### Les Gris d'Émeraude

Groupe de mercenaire appelé par Tomen le chef de Blanchecolline pour se débarrasser des trolls. Ils ont brûlé une grange après que les habitants de Blanchecolline n'est pas de mission à leur confier.

D'après les villageois, ils seraient 5 :

Deux humains, une elfe, un gnome et un demi-géant qui portait une masse dont la tête était formée de deux têtes d'animaux qui ressemblaient à des ours.

Leur chef serait Zatur

### Zatur

Chef supposé des Gris d'émeraude. C'est lui qui a attaqué le village de Donaar

### Zig, le "gobelin-bite"

(H) Gobelin rencontré dans la grotte avant le temple des arcanes. Il a tenté de faire fuir les aventuriers avec des ombres et une grosse voix en se faisant passer pour un dragon. Il s'est avéré plus tard que le gobelin avait été rejeté de sa bande parce qu'il sculptait des os et des bouts de bois en forme de bites ... beaucoup de bites...

### Baldo Ceinturoute

(H) Halfelin cuisinier à la recherche de denrées et recettes rares. Il est accompagné par sa femme Sarya Bouclier-de-Chêne. Il est originaire de Ventreruine. Il revient d'un récent voyage en Estalie. Baldo est un halfelin épais avec une barbe fournie et brune, tressé par endroit. Ses cheveux sont sauvage. Il possède une cicatrice sur l'arcade gauche. Il est en possession d'un fourneau magique qui lui permet de préparer ses succulents plats.

Il a ouvet une auberge à Griffinvale grace à des fonts qu'on lui a donnés

### Sarya Bouclier-de-Chêne

(F) Elfe des bois rôdeuse. Elle est marié à Baldo Ceinturoute. Elle est discrète et réservée. Elle possède une longue chevelure rousse tressé en une queue de cheval épaisse. Elle porte une cuirasse gravée de motifs floraux et des vêtements de cuir dans les tons verts et marron. Elle porte deux lames elfique à sa ceinture ainsi qu'un arc long.

### Draz

(H) Gobelours ancienne esclave. Son clan a été anéanti par Cronak et sa horde, lui a été en parti brûlé et fait esclave. Il a récemment fait un pacte avec Galan et a obtenu des pouvoirs via le diable Kyrsal.

### Hiline Farwissen

(F) Cheveux rasés sur les côtés, court, chataîn. Leader des Esclaves. 

### Irbald Martonnerre

(F) Naine, cheveux tressés, Forgeronne itinérante

• Femme mystérieuse aux cheveux roux et hirsute. Elle porte un foulard orange en guide vêtement. Elle porte à son côté une énorme sacoche.

### Camilla Brillant

(F) Humaine, 25aine, Guerrière-Magicienne, Mercenaire de la Grande Archive de [Griffinvale](.\Wiki\Griffinvale.md). 

### Joda de Grande Manche

Premier ministre de l'empire, il s'occupe des deux soeurs de Galan

### Lajae et Lithel

les soeurs de Galan. Enlevées quand elles étaient petites. Retenues prisonnières par Cabestan pendant plusieurs années

# Port Daran

### Estalar Hadjar

(H) Tabaxi au pelage tigré clair, âge inconnue. Enchanteur des Éclats Ésotériques d'Estalar à [Port Daran](.\Wiki\PortDaran.md). Il a confier à Caleb un morceau d'épée brisée. En échange, il lui a demandé de revenir avec l'épée complètement reforgé.

### Cabestan

Vendeur de bijoux à Port Daran, c'est lui qui retenat prisonnières les deux soeurs de Galan. On l'a laissé en vie mais le voile d'obstidienne s'occupera surement de lui.

## Les copains

# Givrefort

### Orin

Ancien compagnon de donaar quand il était dans la garnison

### Gaston

Ancien compagnon de donaar quand il était dans la garnison

Très con et très drole. Très bourrin aussi

### Gale

Ancien compagnon de donaar quand il était dans la garnison

### Tara

Capitaine des compagnons de Donaar

### Hulfar

Viellaard qu'on a croié dans al vallée des echos. Il était en train de galéré quad on l'a rencontré

# REBELLE DU BASTION ORC 

*Ils ont décidé de suivre les Ombres Éclatantes dans leur quête*

### Damari

(F) Demi-elfe**

**

### Brex

(H)

### Ojer

(H) Halfelin

### Fabron

(H)

### Ola

(F)

### Tibald

(H)

### Kari

(F)

### Nestor

(H)

### "Tonnerre"

(H) il a abandonné son ancien prénom pour devenir "Tonnerre"

# Norska

## Godol

Pisteur rencontré dans Norska, il doit nous conduire au toit du monde

## Camilla

[Camilla Brillant](#Camilla-Brillant)
