# Chemin du dossier contenant les fichiers .txt
$dossier = Get-Location

# Parcours de tous les fichiers .txt dans le dossier
Get-ChildItem -Path $dossier -Filter *.md | ForEach-Object {
    # Lecture du contenu du fichier
    $contenu = Get-Content -Path $_.FullName
    # Remplacement de "aaa" par "bbb"
    $chaine1 = "\(.\\"
    $chaine2 = "(.\Wiki\"
    $nouveauContenu = $contenu -replace $chaine1, $chaine2
    # Écriture du nouveau contenu dans le fichier
    $nouveauContenu | Set-Content -Path $_.FullName
}