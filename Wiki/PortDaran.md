**LE CORGNIAU**

**• La Barque** . Taverne (modeste)

**• Concoctions** **& Potions en tout genre** . Apothicaire

**• Le Plumage** . Librairie

**LE CORDEAU**

**• Les** **Éclats Ésotériques d'Estalar .** Boutique de Magie

*Estalar [Hadjar](.\Wiki\PNJ.md), Tabaxi, Enchanteur*

**• Épée Soyeuse** - Forge

**• Tamia, Bijoux & Joyaux -** Joaillerie

**• L'****Empoisonneur** . Apothicaire

**• Arche de Platine** Temple de Bahamut

**• Le Marché couvert .** Entrepôt et Marché couvert

**L'ABARRETTE**

**• Hall de justice de Port Daran** . Hall de Justice

**• Le Guerroyeur** . Caserne

**• Le Pied de Chaise** . Auberge (modeste).

**L'ESTAMPE**

**• Mots Éparpillés** - Librairie

**• L'Aiguille** **solaire .** Cathédrale de Pelor

**• Le Cercle Nordique** - Auberge (confortable)

**LE BAROUDEUR**

**• La Sudiste** . Caserne

• **La Chouette Étourdie .** Taverne (pauvre)

**LE** **CALCOMAC**

**• Le Vecteur -** Boutique de Bijoux

*Ébra Cabestan**, Humain, 40aine*

**• Chez Giovanni** - Armateur

**• La** **Cabine** - Taverne (pauvre)

**• Remèdes** **divers et variées** - Apothicaire

**• Phare**
