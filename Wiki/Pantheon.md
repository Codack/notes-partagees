**DIEUX BONS**

## Ao

le Seigneur Dieux, le Père Créateur 

## Bahamut

le Dragon de Platine (Loyal Bon)

Domaine de la vie et de la guerre

Bahamut est le dieu de la justice, de la protection, de la noblesse et de l'honneur. Les paladins le vénèrent souvent, et les dragons métalliques le vénèrent comme le premier du genre. Les monarques sont couronnés en son nom. Il commande ainsi à ses disciples :

*Soutenir les plus hauts idéaux d'honneur et de justice.*

*Soyez constamment vigilant contre le mal et opposez-vous à lui sur tous les fronts.*

*Protégez les faibles, libérez les opprimés et défendez l'ordre et la justice.*

## Moradin

le Forgeur d'Âme (Loyal Bon)

Domaine du savoir et de la guerre

Moradin est le dieu de la création et le patron des artisans, notamment des mineurs et des forgerons. Il a sculpté les montagnes de la terre primordiale et est le gardien et le protecteur du foyer et de la famille. Des nains de tous les horizons le suivent. Il exige ces comportements de ses disciples :

*Rencontrez l'adversité avec stoïcisme et ténacité.*

*Faites preuve de loyauté envers votre famille, votre clan, vos dirigeants et votre peuple.*

*Efforcez-vous de marquer le monde, un héritage durable. Faire quelque chose qui dure est le plus grand bien, que vous soyez un forgeron travaillant dans une forge ou un souverain créant une dynastie.*

## La Reine Corneille

 (Loyal Neutre)

Domaine de la vie et de la mort

Le nom de la déesse de la mort est oublié depuis longtemps, mais elle s'appelle la Reine Corneille. Elle est la fileuse du destin et la protectrice de l'hiver. Elle marque la fin de chaque vie mortelle et les personnes en deuil l'invoquent lors des funérailles, dans l'espoir de protéger les défunts de la malédiction du non-mort. Elle attend de ses fidèles qu'ils se conforment à ces commandements :

*Ne craignez rien pour ceux qui souffrent et meurent, car la mort est la fin naturelle de la vie.*

*Abattez les orgueilleux qui tentent de jeter les chaînes du destin. En tant qu'instrument de la Reine Corneille, vous devez punir l'hubris là où vous le trouvez.*

*Surveillez les cultes d'Orcus et éliminez-les chaque fois qu'ils se présentent. Le prince démon des morts-vivants cherche à revendiquer le trône de la Reine Corneille.*

## Erathis

la Porteuse de loi (Loyal Neutre)

Domaine du savoir

Erathis est la déesse de la civilisation. Elle est la muse de la grande invention, fondatrice des villes et auteure des lois. Les souverains, les juges, les pionniers et des citoyens dévoués la vénèrent. Ses lois sont nombreuses mais leur objectif est simple :

*Travaillez avec les autres pour atteindre vos objectifs. La communauté et l'ordre sont toujours plus forts que les efforts décousus d'individus isolés.*

*Apprivoisez les étendues sauvages pour les rendre apte à l'habitation et défendez la lumière de la civilisation contre l'obscurité qui l'envahit.*

*Cherchez de nouvelles idées, de nouvelles inventions, de nouvelles terres à habiter, de nouvelles étendues sauvages à conquérir. Construisez des machines, construisez des villes, construisez des empires.*

## Taranis

Dieu de la stratégie et de l'art militaire. Fils d'Erathis et de Kord. Son culte est usurpé par Adaka, lieutenant de Lloth.

## Pelor

le Père de l'Aube (Neutre Bon)

Domaine de la vie et de la lumière

Dieu du soleil et de l'été, Pelor est le gardien du temps. Il soutient ceux qui sont dans le besoin et s'oppose à tout ce qui est mal. En tant que seigneur de l'agriculture et des récoltes abondantes, il est la divinité la plus communément vénérée par les humains ordinaires et ses prêtres sont bien reçus partout où ils vont. On retrouve des paladins et des rôdeurs parmi ses fidèles. Il dirige ainsi ses disciples :

*Soulager la souffrance partout où vous la trouverez.*

*Amenez la lumière de Pelor dans des endroits sombres, faisant preuve de gentillesse, de miséricorde et de compassion.*

*Soyez vigilant contre le mal.*

## Mélora

la Mère Sauvage (Neutre)

Domaine de la nature et de la tempête

Mélora est la déesse des étendues sauvages et de la mer. Elle est à la fois la bête sauvage et la paisible forêt, le tourbillon déchaîné et le désert tranquille. Les rôdeurs, les chasseurs et les elfes la vénèrent. Les marins lui font des offrandes avant de commencer leurs voyages. Ses commandements sont les suivants :

*Protégez les lieux sauvages du monde contre la destruction et la surutilisation. Opposez-vous à la prolifération des villes et des empires.*

*Chassez les monstres aberrants et autres abominations de la nature.*

*Ne craignez pas et ne condamnez pas la sauvagerie de la nature. Vivez en harmonie avec elle.*

## Ioun

la Maîtresse du Savoir (Neutre)

Domaine du savoir

Ioun est la déesse de la connaissance, de l'habileté et de la prophétie. Les sages, les voyants et les tacticiens la vénèrent, à l'instar de tous ceux qui vivent de leurs connaissances et de leur puissance mentale. Corellon est le patron de la magie des arcanes, mais Ioun est la patronne de son étude. Les bibliothèques et les académies de magie sont construites en son nom. Ses commandements sont aussi des enseignements :

*Recherchez la perfection de votre esprit en mettant en équilibre la raison, la perception et l'émotion.*

*Accumulez, préservez et diffusez les connaissances sous toutes leurs formes. Poursuivez l'éducation, construisez des bibliothèques et recherchez des traditions perdues et anciennes.*

*Soyez vigilant à tout moment pour les adeptes de Vecna, qui cherchent à contrôler la connaissance et à garder des secrets. Opposez-vous à leurs stratagèmes, démasquez leurs secrets et aveuglez-les de la lumière de la vérité et de la raison.*

## Corellon

le Coeur de l'Arche (Chaotique Bon)

Domaine de la lumière

Dieu du printemps, de la beauté et des arts, Corellon est le patron de la magie des arcanes et des fées. Il a semé le monde avec la magie des arcanes et a planté les plus anciennes forêts. Les artistes et les musiciens l'adorent, de même que ceux qui considèrent leur lancer de sorts comme un art et ses sanctuaires se retrouvent dans tout la Féerie. Il méprise Lolth et ses prêtresses pour avoir égaré les elfes noirs. Il exhorte ainsi ses disciples :

*Cultivez la beauté dans tout ce que vous faites, qu'il s'agisse de lancer un sort, de composer une saga, de jouer du luth ou de pratiquer les arts de la guerre.*

*Cherchez des objets magiques perdus, des rituels oubliés et des œuvres d'art anciennes. Corellon aurait pu les inspirer lors des premiers jours du monde.*

*Contrecarrez les adeptes de Lolth à chaque occasion.*

## Avandra

le Porteuse de Changement (Chaotique Bon)

Domaine de la duperie

Déesse du changement, Avandra aime la liberté, le commerce, les voyages, l'aventure et les frontières. Ses temples sont peu nombreux dans les pays civilisés, mais ses sanctuaires au bord de la route apparaissent dans le monde entier. Halfelins, marchands et tous les types d'aventuriers sont attirés par son culte. De nombreuses personnes lèvent un verre en son honneur et la considèrent comme la déesse de la chance. Ses commandements sont peu nombreux :

*La chance favorise les audacieux. Prenez votre destin en main et Avandra vous sourira.*

*Ripostez contre ceux qui voudraient vous priver de votre liberté et exhortez les autres à se battre pour leur propre liberté.*

*Le changement est inévitable, mais il faut le travail des fidèles pour s'assurer que le changement est pour le mieux.*

## Sehanine

la Tisseuse de Lune (Chaotique Bon)

Domaine de la duperie

Déesse de la lune et de l'automne, Sehanine est la patronne de la tromperie et des illusions. Elle a des liens étroits avec Corellon et Mélora et est une divinité préférée des elfes et des halfelins. Elle est aussi la déesse de l'amour, qui donne des capes d'ombres aux amoureux intrépides. Les éclaireurs et les voleurs lui demandent de bénir leur travail. Ses enseignements sont simples :

*Suivez vos objectifs et recherchez votre propre destin.*

*Restez à l'ombre, en évitant la lumière flamboyante du bien zélé et l'obscurité totale du mal.*

*Cherchez de nouveaux horizons et de nouvelles expériences et ne laissez rien vous lier.*

## Kord

le Seigneur des Tempêtes (Chaotique Neutre)

Domaine de la tempête et de la guerre

Kord est le dieu de la tempête et le seigneur du champ de bataille. Il admire la force, les prouesses au combat et le tonnerre. Les guerriers et les athlètes le vénèrent. C'est un dieu d'humeur changeante, débridé et sauvage, qui convoque des tempêtes sur terre et sur mer; ceux qui espèrent un temps plus clément l'apaisent par des prières et trique en sont honneur. Il donne peu de directives :

*Soyez fort, mais n'utilisez pas votre force pour une destruction aveugle.*

*Soyez courageux et méprisez la lâcheté sous n'importe quelle forme.*

*Prouvez votre force sur le champ de bataille pour gagner la gloire et la renommée.*



**DIEUX MAUVAIS**

## Asmodée

le Seigneur des Enfers (Loyal Mauvais)

Domaine de la duperie

Asmodée est le dieu maléfique de la tyrannie et de la domination. Il dirige les Neuf Enfers avec une poigne de fer et de beaux discours. Outre les diables, des créatures perverses telles que Rakshasa lui rendent hommage. Les Tieffelins corrompus et les Sorciers sont attirés par ses cultes sombres. Ses règles sont strictes et ses punitions sévères :

*Cherchez le pouvoir sur les autres, afin de pouvoir gouverner avec force, comme le fait le Seigneur de l'enfer.*

*Combattez le mal avec le mal. Si les autres sont bon avec vous, exploitez leur faiblesse pour votre propre profit.*

*Ne montrez ni pitié, ni merci à ceux qui sont à vos pieds alors que vous grimpez le chemin du pouvoir. Les faibles ne méritent pas la compassion.*

## Tiamat

le Tyran d'Écailles (Loyal Mauvais)

Domaine de la duperie et de la guerre

Tiamat est la déesse maléfique de la richesse, de l'avidité et de l'envie. Elle est la protectrice des dragons chromatiques et de ceux dont l'envie de gagner l'emporte sur tout autre objectif ou préoccupation. Elle commande à ses partisans de :

*Accumulez de la richesse, acquérir beaucoup et dépenser peu. La richesse est sa propre récompense.*

*Ne pardonnez pas et ne laissez pas de torts impunis.*

*Prends ce que tu désires des autres. Ceux qui manquent de force pour défendre leurs biens ne sont pas dignes de les posséder.*

## Baine

l'Empereur de la Discorde (Loyal Mauvais)

Domaine de la guerre

Baine est le dieu maléfique de la guerre et de la conquête. Des nations d'hommes et de gobelins militaristes le servent et conquièrent en son nom. Les combattants et les paladins malfaisants le servent. Il commande à ses fidèles de :

*Ne jamais laissez votre peur prendre le dessus sur vous, mais la propulser dans le cœur de vos ennemis.*

*Punir l'insubordination et le désordre.*

*Affinez vos compétences au combat à la perfection, que vous soyez un général puissant ou un mercenaire solitaire.*

## Torog

le Roi Rampant (Neutre Mauvais)

Domaine de la mort

Torog est le dieu maléfique de l'Outreterre, patron des geôliers et des bourreaux. La superstition est bien répandue: si son nom est prononcé, le Roi Rampant creuse sous le sol et entraîne le malheureux orateur sous terre pour mener une éternité d'emprisonnement et de torture. Les geôliers et les tortionnaires le prient dans des cavernes et des caves profondes, et les créatures de l'Outreterre le vénèrent également. Il enseigne à ses fidèles à:

*Cherchez et vénérez les profondeurs sous la terre.*

*Prenez plaisir à donner de la douleur et considérez la douleur comme un hommage à Torog.*

*Attachez bien ce qui est sous votre responsabilité et restreignez ceux qui errent librement.*

## Lloth

la Reine Araignée (Chaotique Mauvais)

Domaine de la duperie

Lolth est la déesse maléfique chaotique de l'ombre, des mensonges et des araignées. Ses ordres sont des intrigues et des trahisons, et ses prêtresse sont une force constante de perturbation dans la société, par ailleurs stable, elfe noir. Bien qu'elle soit proprement une déesse et non un démon, elle s'appelle La Reine Démone Araignée. Elle demande à ses partisans :

*Faites ce qu'il faut pour obtenir et conserver le pouvoir.*

*Fiez-vous à la furtivité et la calomnie plutôt que sur la confrontation pure et simple.*

*Cherchez la mort des elfes à chaque occasion.*

## Gruumsh

le Ravageur (Chaotique Mauvais)

Domaine de la guerre et de la tempête

Gruumsh est le dieu maléfique chaotique de la destruction, seigneur des hordes en maraudage. Là où Baine commande la conquête, Gruumsh encourage ses fidèles au massacre et au pillage. Les Orcs sont ses fervents partisans et ils détestent particulièrement les Elfes parce que Corellon a crevé un œil de Gruumsh. Le dieu borgne donne des ordres simples à ses disciples :

*Conquérir et détruire.*

*Laissez votre force écraser les faibles.*

*Faites comme vous voulez, et que personne ne vous arrête.*

## Zehir

le Serpent Caché (Chaotique Mauvais)

Domaine de la duperie et de la mort

Zehir est le dieu maléfique des ténèbres, du poison et des assassins. Les serpents sont sa création préférée, et les Yuan-Ti le vénère avant tous les autres dieux, lui offrant des sacrifices dans des fosses pleines de serpents qui se tordent. Il encourage ses partisans à:

*Cachez-vous sous le manteau de la nuit pour que vos actes restent secrets.*

*Tuez au nom de Zehir et offrez chaque meurtre en sacrifice.*

*Délectez-vous du poison et entourez-vous de serpents.*

## Tharizdun

les Chaînes de l'Oubli (Chaotique Mauvais)

Domaine de la duperie

Tharizdun est le dieu maléfique chaotique qui a créé les Abysses. Quelques cultes dispersés d'adeptes déments le vénèrent, l'appelant le Dieu Enchaîné ou Œil d'Élémentaire Aîné. Tharizdun ne parle pas à ses partisans, ses commandes sont donc inconnues, mais ses sectes enseignent à leurs membres à:

*Canalisez le pouvoir au Dieu Enchaîné pour qu'il puisse briser ses chaînes.*

*Récupérez les reliques et les sanctuaires perdus auprès du Dieu Enchaîné.*

*Poursuivre l'oblitération du monde, en prévision de la libération du Dieu Enchaîné.*

## Vecna

le Roi Immortel (Neutre Mauvais)

Domaine de la mort et du savoir

Vecna est le dieu maléfique des morts-vivants, de la nécromancie et des secrets. Il occulte ce qui n'est pas censé être connu et ce que les gens souhaitent garder secret. Des lanceurs de sorts et des conspirateurs malins lui rendent hommage. Il leur commande de:

*Ne révélez jamais tout ce que vous savez.*

*Trouvez la graine des ténèbres dans vos cœurs et nourrissez-la; trouvez-la chez d'autres et exploitez-la à votre avantage.* 

*Opposez-vous aux adeptes de toutes les autres divinités afin que Vecna puisse à lui seul gouverner le monde.*
