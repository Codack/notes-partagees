**LA CITADELLE**

*Forteresse de l'Empereur et sa cour*

**L'ÉTENDARD**

**• La Volute Dorée** - Salle de jeu

**QUARTIER DU HUNVAR**

• **Au dodo du Roy** - Taverne confortable

**LA GRANDE PORTE**

• **Le Hall de Justice de Griffinvale**

**LE FORGELIEU**

• **La Grande Archive**

Erica Urnemage, (F), Demi-elfe, Conservatrice de la Grande Archive (Tête de l'organisation)

Cercle de Téléportation

• **Le Feu et Les Potions** - Apothicaire et Alchimiste

Kavar, (H) Humain, Pilosité importante et sauvage, ton calme

• **Gemmes et gemmes** - Joaillier

• **Les Précieuses Pierres** - Joaillier chic

• **Chez Jopec** - Joaillier sombre

• **Lumière Précieuse** - Joaillerie exotique

**QUARTIER DU CAVALIER**

• **Académie du Solstice -** Académie de Magie

L'Académie est sous la tutelle de l'Assemblée du Cerbère

• **Siège de l'Assemblée du Cerbère**

*Composé de nombreuses tours de mages connectées entre elles, sur le campus de l'Académie du Solstice*

• **Académie des Vestiges -** Académie d'archéologie, d'archivage et d'histoire

**LE FANION**

• **École du Griffon** - École Militaire

• **Caserne de l'Armée du Griffon**

**L'ALFANIR**

**• Docks**

*Ils donnent accès aux fleuves Le Flux qui traverse Griffinvale et se jette dans l'Océan d'Émeraude à Fort Écarlate*

**LA RAMBARDE**

*Bas-fonds de la ville*
