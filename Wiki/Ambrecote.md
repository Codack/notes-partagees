Ville de passage. Elle est située sur la Route du Sud entre Dramacna et l'Empire, une imposante route commerciale qui remonte jusqu'à [Arcvale](.\Wiki\Arcvale.md). 

**LA BASSE-COLLINE**

• **Le Pichet renversé** . Taverne proche de l'entrée de la ville , modeste

• **Le Hall de justice**

• **La Maison du Réconfort** . Hopital

• **Le Parchemin brûlé** . Librairie

*Camille Bustefeu, Drakéide bronze*

• **Autel de Erathis**

*Statue d'une jeune femme, portant à bout de bras une roue cranté, elle est en armure et porte un livre dans son autre main.*

**LA COLLINE**

*Quartier riche de de la ville, l'accès à ce quartier est restreint*

• **La Caverne aux Coussins .** Auberge riche

**LE DÔME**

• **Les Plumes du Val .** Boutique de tisserand

• **La Tour d'Esbrine .** Hôtel de ville

**LE QUARTIER DES ÉCHANGES**

• **Place du Temple .** Place 

• **La Forgétoile .** Forge

• **Le Sac énergisant .** Apothicaire

• **L'atelier de Grombroc .** Forge

*Gromboc Barbacier, nain*

• **Temple d'Erathis .** Temple

• **Au Luth Fracassant** : Atelier de menuiserie

• **Le Temple du Seigneur de Platine** : Temple de Bahamut

**LA TOURBIÈRE**

*Quartier pauvre de la ville*

• **Le Repos Mérité** : Auberge miteuse, au dessus du repaire à une organisation criminel

*Tarjac, halfelin, 40aine*

• **Les Écuries de Jim** : Écurie
