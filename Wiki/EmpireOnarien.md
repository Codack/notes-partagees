**COURONNE :**

**• Oskar II Onari :** Humain (H) (15 ans), Empereur de l'Empire Onarien. Peau claire, cheveux long et blond attachées en tresses complexe. Il possède un grain de beauté au dessus de l'arcade gauche.

• **Valdo Exqui****s** : Humain (H) (50aine) Tuteur de l'Empereur

• **Joda De Grandemanche** : Elfe (H) Œil de l'Empire (Premier conseiller de l'Empereur) Elfe, Homme (210 ans) Cheveux raide noir de geai long, longue toge/robe, verte claire par endroit et blanche. Bijoux sobres et discret. Broche du symbole de l'Empire : Calice avec des lauriers . Petites lunettes

**ASSEMBLÉE DU CERBÈRE :** 

**Emblème de l'Assemblée du Cerbère** : Trois cristaux losanges

C'est une assemblé de mages exerçant un pouvoir politique. Elle assure la régence de l'Empire. Ils ont une place au conseil de l'Empereur. C'est une haute autorité de la magie et l'Académie du Solstice dépend directement d'elle. Le siège de l'Assemblé se trouve à Griffivale dans le Quartier des Orbes. Les mages de cette assemblé se font nommé Archimages.

**• Althéa Galanodel** : Elfe (F) Archimage de l'Assemblée du Cerbère dont elle est la chef. Elle se tient droite et porte une tenue d'érudit de couleur bleuté. Elle a des cheveux courts et argentés. Elle semble être dans la force de l'âge. Elle est la régente de l'Empire et gouverne de pair avec l'Empereur.

• **Sylthana Adnala**, Elfe, Femme (115 ans) Cheveux long blond clair tressé d'un côté, lunettes d'archiviste, tenue bleuté robe de mage, Brassard enchanté, Jeune. Archimage des Antiquités (Archimage de l'Assemblée du Cerbère) :

**GRANDE ARCHIVE DE GRIFFIVALE :**

• **Camila Brillant** : (F) Humaine, 25aine, Guerrière-Magicienne, Mercenaire de la Grande Archive de [Griffinvale](.\Wiki\Griffinvale.md). 

Elle possède un morceau d'armure avec des runes et des pierres précieuses. Elle a perdu un œil lors d'une expédition, à cause d'un piège mal désamorcé.

***•\* Erica Urnemage :** (F) Demi-elfe, femme (110 ans). Conservatrice (Tête de la Grande Archive de [Griffinvale](.\Wiki\Griffinvale.md)) ; Cheveux sombre parsemé de blanc attaché en un chignon de tresse complexe. Visage légèrement ridé. Habit de couleur bleuté bordé de fil d'argent avec des paternes géométrique. Elle possède un long manteau qui va jusqu'au cheville. Foulard blanc au paterne géométrique. Parchemins et sacoche au côté. Lunette d'Archiviste.

Elle a donné à Saphir et par extension au Ombres Éclatantes, la droit d'utiliser les cercles de téléportations du réseau de la Grande Archive en échange des informations du Bastion des Nains dans lequel, ils s'apprêtent à aller.

**[GRIFFINVALE](.\Wiki\Griffinvale.md) :**

• **Majeff Miremasque** ,Voix de Dramacna ,Nain, Homme (180 ans) Chauve rasé , Longue barbe tressé qui par du blanc pour aller vers une roux clair. Bijoux ostentatoires : bracelets, bagues. Pipe en bois sculpté au côté. Bedonnant. Broche à l'effigie de l'emblème de Dramacna : Bateau à 2 mâts avec des flammes en guise de vigie sans voile. Habit bourgeois 

• **Avoril Marchetemps** , Commandant de l'Armée du Griffon (Armée de [Griffinvale](.\Wiki\Griffinvale.md)), Demi-elfe (F), 127 ans. Armure de cérémonie. Stratège et prudent mais avare. Cheveux long tressés grisonnant . Faible pilotié.

• **Okira Kulmadas**, Capitaine de l'Armée du Griffon (Armée de [Griffinvale](.\Wiki\Griffinvale.md)), Femme Drakéide, cicatrices un peu partout sur le corps, armure lourde, épée bouclier lance, symbole de bahamut autour du cou. Calme, tactique

• **Kelra Tracepierre** , Capitaine de la Guilde des Rôdeurs de l'Empire , Humaine (F), 30aine, Cheveux rasées cicatrices sur le visage, armure de cuir abîmée, capuche, tissus de couleur vert, Foulard épais, (tenue d'hiver) fourrure sur les épaules.

• **Ivan Martyr,** Représentant de la Haute Autorité des Guildes , Humain (H), 60aine, Long cheveux gris attaché, calvicie prononcée au niveau du front. Habits bourgeois, registre. Patriache de la Famille Martyr. Dirigeant de la Guilde des Marchands de [Griffinvale](.\Wiki\Griffinvale.md) et représentant des Guildes de l'Empire.

**FORT ÉCARLATE :**

• **Voltura Ambrecoeur** : (F) Humaine, 40aine, Porte-Justice de Fort Écarlate

**AMBRECÔTE :**

• **Léo Maximilian** : (H) Humaine, 40aine, Capitaine de la Garde
