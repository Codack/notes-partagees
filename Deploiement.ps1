$file = Join-Path $PSScriptRoot "index.html"
$content = Get-Content $file

if ($content | Select-String "abcde") {
	$content = $content -replace "abcde", "fghij"
}
else {
	$content = $content -replace "fghij", "abcde"
}
Set-Content $file $content

git add -A
git commit -m "Déploiement"
git push --progress "origin" master:master