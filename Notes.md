# Darlin Gibrin

## Description

* 1m35 pour 70kg : je suis un solide gaillard

* Cheveux bruns avec une meche blanche
* Brabe relativement courte (pour un nain)
* Yeux bleux
* Peau halée
* Quelques cicatrices sur le corps, marques des aventures que j'ai vécus dans mon passé
* Tatouages
  * Enseigne de la Couronne sur mon avant-bras gauche
  * [Symbole de Moradin ](https://kaimeir.fandom.com/wiki/Moradin)sur mon pec gauche
  * Petit marteau reposant contre une enclume sur mon mollet droit
  * Ligne faisant le tour de mon bras droit au niveau de mon biceps
  * Dans le dos : [symboles](http://realms.dndpedia.com/index.php?title=Morndinsamman) de tous les nains du [Morndinsamman](https://forgottenrealms.fandom.com/wiki/Morndinsamman) avec le symbole de Moradin en plus gros au dessus (sans les nains maléfique évidemment)
* Equipement
* Marteau de guerre avec le symbole de Moradin à la base de la tête
* Armure avec une cape blanche et symbole argenté de Moradin (amovible pour quand on veut être discret)
* Bouclier avec symbole de Moradin

## Résumé backstory

Je suis né à Grisebay, petit village de pêcheur. J'ai été élevé par un nourrice du nom de Leeda. A sa mort, je décide de partir vers Gardemont à la recherche de nain. Je rentre dans la communauté de la Couronne d'Angalur, à la fois lieu de culte de Moradin et forge. J'apprends le métier de forgeron ainsi que la foi en Moradin. A l'age de 27 ans, je pars à l'aventure avec les Gardes Lumière. Je me sépare du groupe au bout de 2 ans. Je repars à l'aventure juste après avec comme missions : retrouver le casque de Moradin et reforger l'épée de Taranis.

\- 10 ans : Marret me propose de devenir apprenti forgeron
 \- 14 ans : mort de Leeda => je pars pour Gardemont et je rentre à la Couronne
 \- 25 ans : Gibrin me présente à la guilde des forgerons
 \- 26 ans : appel de Moradin => je vais me battre contre les orcs
 \- 27 ans : je quitte la Couronne pour partir à l’aventure avec les Gardes Lumière
 \- 29 ans : on gagne masse thunes et mes collègues se cassent 
 \- Je repasse à la Couronne + Moradin me dit d’aller chercher son casque et l’épée de Caleb
 \- 30 ans : aujourd’hui

## Lien RP

### Grisebay 
ville d'enfance
### Marret
Homme de 50 ans, forgeron à Grisebay, c’est lui qui m’a initié à l’art de la forge le nom de sa forge est l’enclume ardente

### Koxvar 
Homme de 27 ans, ami d’enfance devenu marin

### Hunrar
Femme de 28 ans, amie d’enfance parti à la recherche de sa tribu barbare

### Thorge

homme de 26 ans, ami d’enfance parti dans une grande ville pour étudier la magie

### Leeda
femme morte, anciennement nourrice à Grisebay.
### Gardemont 
Ya de la contrebande d'arcanite (de la poudre de composant de sort) (tout le monde est plus ou moins au courant)
Ville de mineurs donc pas mal de bijoutier etc... 
Ville très artisants car proche des mines 
Gros héritage nanique/gnome (plus de nain et de gnome que d'humain)

### La couronne d'angalur 
(forge/temple de Moradin)
### Bordar Gibrin
nain de 220 ans, maître forgeron de La Couronne d’Angalur, il m’a apprit à forger proprement et m’introduisit aux mœurs naines
### Agas et Taris
nains de 40 ans, frères jumeaux travaillants à La Couronne d’Angalur. Ils ont joué un rôle de mentors à mon égard pendant mes années à La Couronne
### Les gardes lumières
* Demys, elfe de 25 ans, mage : Toujours occupé par ses bouquins, il ressemble en apparence à n’importe quel noblio sans histoire mais il souhaite en réalité se venger d’un cousin qui l’a humilié dans sa famille. 
* Danea, humaine de 19 ans, guerrière : Elle écrit un livre. L'aventure lui donne l'inspiration et les sous dont elle a besoin mais quand elle a gagné l'argent, elle avait presque finit son livre
* Anman, homme de 22 ans, barabare : Veut amasser des sous pour retourner dans sa tribu et pouvoir la protéger

## Les copains

* Saphir : Plutot discrete donc pas grand chose à dire. Elle connait toute la magie. Elle est beaucoup plus courageuse qu'elle le laisse penser. (ca fait pas lon,gtemps que je la connais mais j'ai l'impression que les copains ont déteint un peu sur elle)

* Caleb : Il a l'air sympatique et il prie un dieu aussi donc ca c'est chouette 
  Paladin de Taranys. C'est un vrai bon on s'est battu ensemble
  Il a tendance à sur-réagir par moment.
  
* Donnaar : Bon bourrin, bon vivant. Mais il retient très peut sa langue "Darlin tais toi". Il prie Bahamut (il reprend parcequ'il avait arreté pendant un moment) donc ca c'est cool. Il tient FORT à nous et aux gens qui lui sont proches

* Leopold :  Turbo casse couille mais il est sacrément efficace en fight. Un peu imature. Il est con mais golri par moment.

* Galan : Galan a fait un pacte avec un démon.
   Il s'appel Nuada de base
   
  > Galan veut se venger de sa famille [Siannodel](./Wiki/Siannodel.md) : qui font du trafic d'être humains. Il s'est enfuit en étant ado à Cornécaille. ya un an il a fait son pacte à un moment de faiblesse. 
  > Heureusement Elyo a réussi à aranger le contrat parce qu'il est cool. Il a fait le contrat en connaissance de cause
  >
   > Il est brisé. On a une vision des choses trop différentes
  
  Le pb c'est quue Kirsal son Démon commence à gagner en puissance mais Galan est pour l'instant maitre de lui meme (C'est Moradin qui me l'a dit)
  
* Moi : je sais pas encore tropo ce que je fout là mais ils ont l'air sympa. Si Moradin m'a guidé ici c'est que j'ai un role à jouer. Moradin a confirmé que j'étais au bon endroit alors je reste.

# Mythologie

## Morndinsamman

[Morndinsamman | Forgotten Realms Wiki | Fandom](https://forgottenrealms.fandom.com/wiki/Morndinsamman)

Terme nain signifiant "Haut frère de bouclier" ou "Hauts nains"

> Nos ancêtres ont insufflé en nous le potentiel de tout ce qui a fait leur grandeur. Il est de notre responsabilité d'affiner ce don pour en faire quelque chose de merveilleux.

### Base d'opération de Morndinsamman : Maison-naine

La pluspart des membres de Morndinsamman résidaient à Maison-naine quand elle était encore à Mont Celestia. 

Maison-naine fut construite par Moradin avec l'aide de sa femme Berronar Vérargent

### Membre de Morndinsamman

#### Moradin

Père de toute chose

Meneur des Morndinsamman, juge dur mais juste. 

> Chaque coup de marteau sur l'enclume, chaque feu allumé dans la forge, est une étape d'un voyage que Moradin lui-même m'a préparé. Ce n'est pas du travail. C'est un défi pour atteindre la grandeur.

[Moradin | Forgotten Realms Wiki | Fandom](https://forgottenrealms.fandom.com/wiki/Moradin)

#### Berronar Vérargent

Femme de Moradin

Appelé "mère révéré" ou "mère de la sécurité" 

[Berronar | Forgotten Realms Wiki | Fandom](https://forgottenrealms.fandom.com/wiki/Berronar_Truesilver)

#### Dmathoïn

[Dumathoin | Forgotten Realms Wiki | Fandom](https://forgottenrealms.fandom.com/wiki/Dumathoin)

Patron des boucliers nains
Dieu des mines, de l'exploration souterraine
Gardien des secrets sous la montagne
Protecteur de nos morts

#### Sharindlar

[Sharindlar | Forgotten Realms Wiki | Fandom](https://forgottenrealms.fandom.com/wiki/Sharindlar)

Déesse des soins et de la miséricorde
Dame de ma miséricorde, dame de la vie, de la bonté
Patronne de l'amour, de la séduction et de la fertilité

#### Clangdinn Barbargent

 [Clangeddin Silverbeard | Forgotten Realms Wiki | Fandom](https://forgottenrealms.fandom.com/wiki/Clangeddin_Silverbeard)

Dieu des batailles, de l'honneur
Père de la battaille et seigneur des haches jumelles

> La guerre est le moment de gloire pour le peuple nain

#### Vergadain

[Vergadain | Forgotten Realms Wiki | Fandom](https://forgottenrealms.fandom.com/wiki/Vergadain)

Appelé "Le dieu rieur"
Dieu de la chance, de la ruse, de la négociation et de la richesse
Parfois considéré comme un exarque de Moradin 

#### Dugmaren Manteaubrillant

[Dugmaren Brightmantle | Forgotten Realms Wiki | Fandom](https://forgottenrealms.fandom.com/wiki/Dugmaren_Brightmantle)

Dieu mineur de l'apprentissage et de l'innovation
Patron des écoles et penseurs libres
Egalement dieu du savoir (peu importe son utilité)

#### Gorm Gulthyn

[Gorm Gulthyn | Forgotten Realms Wiki | Fandom](https://forgottenrealms.fandom.com/wiki/Gorm_Gulthyn)

Dieu mineur de la vigilance et de la défence
Patron des nains qui protège leur demeur

#### Haela Hachebrillante

[Haela Brightaxe | Forgotten Realms Wiki | Fandom](https://forgottenrealms.fandom.com/wiki/Haela_Brightaxe)

Demi-déesse de la battaille et de la chance

> Parce que c'était là, et parce que j'ai toujours voulu me frayer un chemin dans l'estomac de quelqu'un.

-> clerc de Haela après qu'on lui ai demandé pourquoi elle avait attaqué un dragon ancien

#### Marthammor Duin

[Marthammor Duin | Forgotten Realms Wiki | Fandom](https://forgottenrealms.fandom.com/wiki/Marthammor_Duin)

Dieu des vagabons, de ceux qui ont laissé leur clan pour explorer le monde

#### Thard Harr

[Thard Harr | Forgotten Realms Wiki | Fandom](https://forgottenrealms.fandom.com/wiki/Thard_Harr)

Dieu de la survie, de la chasse
Il a vécu au mileu de bête pendant longtemps
Patron des nains sauvages

#### Abbathor

[Abbathor | Forgotten Realms Wiki | Fandom](https://forgottenrealms.fandom.com/wiki/Abbathor)

Dieu de la cupidité
"Le grand maître de la cupidité"
Dieu mauvais 

#### Hanseath

[Hanseath | Forgotten Realms Wiki | Fandom](https://forgottenrealms.fandom.com/wiki/Hanseath)

Dieu mineur de la fête, de la chanson et de l'alcool 

#### Tharmekhul

[Tharmekhûl | Forgotten Realms Wiki | Fandom](https://forgottenrealms.fandom.com/wiki/Tharmekh%C3%BBl)

Demi-dieu de la forge et du fourneau

***

Même si ils faisaient parti de Morndinsamman au départ, les deux dieux duergar en fuent bannis par Moradin

#### Laduguer

[Laduguer | Forgotten Realms Wiki | Fandom](https://forgottenrealms.fandom.com/wiki/Laduguer)

Appelé "L'exilé", "Le protecteur gris", "Maitre d'oeuvre"

#### Duerra la profonde

[Deep Duerra | Forgotten Realms Wiki | Fandom](https://forgottenrealms.fandom.com/wiki/Deep_Duerra)

Demi-déesse de la conquête, de l'expansion
Reine de l'art invisible 

### Histoire

Morndinsamman mena une croisade dans le but de tuer Laduguer et Duerra et c'est pendant ces combats, il est dit que Moradin perdit son casque.

## Autres

### Mont Celestia

Les Sept Cieux du Mont Célestia, également appelés les Sept Montagnes de la Bonté et de la Loi, les Sept Cieux du Mont Célestia ou les Sept Cieux, étaient le sommet de la bonté loyale dans la cosmologie de la Grande Roue. Les Sept Cieux étaient dédiés à la bonté façonnée par la loi et à la justice tempérée par la miséricorde. Certaines caractéristiques de ce plan ont été attribuées aux plans de la cosmologie de l'Arbre Mondial des Champs Verts et de la Maison de la Triade lorsque cette cosmologie est devenue populaire. Dans la cosmologie de l'Axe Mondial, Célestia a été interprétée comme un dominion astral gouverné par Torm alors qu'il dérive à travers la Mer Astrale.  

### Amegrim

[Hammergrim | Forgotten Realms Wiki | Fandom](https://forgottenrealms.fandom.com/wiki/Hammergrim)

Amegrim était le plan d'origine de Laduguer et de Duerra la profonde dans la cosmologie de l'Arbre-Monde. Le dieu gnome maléfique Urdlen a également creusé ses tunnels à travers les fondations du plan.


# Notes JDR

## Koxvar

Blanche colline : on a tué les trolls et les villageois nous adorent

### Fort écarlate

On va à fort ecarlate et on croise le reuf de Galan "Zeblis"
On investigue au solarium pour trouver les coupables "de taranis"
On trouve un navir pour remonter jusqu'à Ombrelune La girouette, Balmur Epicau
On est descendu pour buter des crabes sur intel de Dovan
J'ai trouvé un anneau


On explore la grotte des hommes-poissons 
Je cast fireball sur la stèle et un mot m'arrive dans la tête le mot c'est en elfique "drekalar"

On continue l'explo, on tue une grenouille chelou à tentacules, on loot plein de truc, on sort et Donaar nous avait attaqué. Nawelis me donne l'amulette de cicatrisation

On finit les souterrains on bute 21 hommes poissons

Caleb a eu une vision, pour retrouver son malfaiteur
On charge La girouette et Balmur vient manger avec nous

### Ile de Caleb

On se bat contre le serpent des mers de ouf
Balmure a été rejeté de chez lui et il kiff la mer comme moi
Ils nous laissent sur l'ile 
On se bat aussi contre des hommes lezard

On monte dans le volcan pour buter Rabjack
Au moment ou j'immole nos ennemis je ressent la présence d'une entité maternelle

On se casse au moment ou un dragon rouge arrive sur l'ile 
On ramène le bateau :
Le goélant, Ebole Moudouc (halfelin, renard masqué)
On rend le bateau à Ebole contre 200po par mois pendant 3 ans

### Arcvale et les Syanodel

Galan se fait trahir par Zeblis
On décide de partir vers Arcvale puis Ombrelune (chacun sa merde)
On va reprendre la girouette
On fait le trajet

On est les Ithilhûg
Jortar c'est mon nom
On arrive à Arcvale
Elrond : fils d'un riche marchand venant de l'empire Tudo qui souhaite importer son commerce : besoin de connexion... Dont les Deignass et des Sianodel
Aller voir : Simon Julinius (tenancier de l'auberge)
Rumeur : type qui voulait tuer les Titarion (c'est moi en vrai lol)
Galan a envoyé un message pour les titarions 

On va à la soirée, on remarque un homme très poilu avec une grosse épée
Ya Alina VonAften qui drague Galan
Ya Oscar Deignass qui drague Saphir
Ya des bails entre Saphir et Oscar et Galan fait un accord avec les Deignass
J'avais volé 2 teille de rouge 

### Plan du feu

On rentre à l'Auberge 
J'ai pris une flasque de liqueur Sianodel
On a une réponse de Fargas Brinmorne
On va dans les bois dans le temple de la voie des flammes
Fargas dément l'histoire de Ander
Je croise le regard de deux types dans le temple 
On assiste à une cérémonie au temple du cul
On se fait aspiré par un portail vers le nether

Je vois un enfant un un oiseau aux ailes rougeatre côte à côte 
Je me suis re-bruler les bras
C'était mes darons je m'appelais "Icar Titarion"
Nouvelle histoire : "Quand j'étais pas né : voyageur à Arcvale, mes darons S'étaient disputés et elle était enceinte les affaires allaient mal et ce voyageur les a aidé, quand elle a acouché, je semblais etre réceptif de la pierre du voyageur, après quelques années, la veille de ma disparition, je jouais avec Fargas et la pierre, une gerbe de feu est sorti de ma main, quelqu'un m'a pris, ander était pote avec mon reup, elle a pas l'aire d'etre d'accord avec la version d'Ander, le phoenix c'est le symbole des Titarion, d'après Aoth, son père lui avait parlé d'une histoir avec un phoenix venant d'un ancetre" (J'ai aussi un oncle Tavarne)
On marche vers une tour dans le nether et je discute avec mes darons et Fargas sur le chemin
C'est Fargas qui a essayé de nous buté (Aoth etNephis n'en savaient rien) il a rien fait pour me transformer en maudit
Fargas est la dernière des saloperie sur terre et Aoth a l'air plutot d'accord avec moi
J'ai une soeur qui s'appelle Missa
On tombe sur des serpents de feu du cul
On se réfugie dans une tour ou on est un peut mieux

On monte dans la tour pour voir la tempete qui arrive : comprenez "c'est la merde"
J'entends une voix d'enfant comme un gros tazré
J'entends un oiseau 
On entend tous le cri d'un énorme oiseau probablement de phoenix
Le cris vient du haut de la montagne 
Je dois aller en haut de la montagne à 10 mille %
Fargas est vraiment sacrément taré il va y passer sûr 
On passe la tempete 
On se met en route vers la montagne
On tombe sur une rivière de lave avec des bestioles bizarres à qui pour changer on essaye de parler PUIS on leur pète la gueule 
Les connards meurent quasiment tous dans la lave en traversant (Fargas brule ce gros fdp)

On marche jusqu'à une falaise
On galère sa mere à escalader mais tout le monde passe
On entend un bruit d'eau en haut de la falaise 
Dans la cave on se fait attaquer par un sucube 
J'attaque au passage tout mes potes avec une jolie boule de feu
Galan a fumé Nawelis qui s'est faite achevée par la bestiole
Saphir a rez Nawewe grace à la piece de résurection 
J'ai ENCULE la bestiole à base d'un "léger" 90 de dégats
On se remet à marcher et on arrive à un gigantesque cratere
Ya plein d'oeuf et une énorme sphère de feu au milieu
Quand je m'approche de la sphère ya un putain de phoenix qui sort et nous gueule dessus il a l'aire ultra vénère

VISION :
Je vois la gamine jouer avec le phenix blesser, le phenix lui soufle dessus et elle a des braises dans le yeux
Et phenixia qui se bat contre Zorgoth (Zorgoth perd une main) la main de Zorgoth est tombée dans le sable et perdu 
Phoenixia 
Humain porteur de la bénédiction des phoenix Koxvar enfant du phenix manipulateur du feu sauvage soit un sacré BG
Je suis une seigneur" du feu  native du polan, autrefois par un des ses frere Zorgoth lame de braise. "
"a tu deja entendu le nom de Zorgoth?"
Elle soigne mes brulures les plus récentes
Son frère Zorgoth va probablement la buter quand elle sera sous forme d'oeuf mais elle me dit aussi que je peux rien y faire pour l'instant 
Je lui promet de prendre soin de l'oeuf qu'avait Fargas
Elle ouvre un portail et on rentre dans le plan normal
Je tombe sur l'oeuf de fargas et il éclos devant moi
Ya un brassar dans l'oeuf trop stylé
Ya un bébé phoenix dans l'oeuf aussi et je l'appelle "Lueur"
Aoth me parle dans son bureau et nous file la clef d'une maison dans Arcvale
En discutant avec ma famille Missa kiff l'aventure et voudrait partir en bateau
Oncle en tante vivants, Tavarn parti en Estali et Ika marié avec un Alchimiste de Gardemont 

### Retour du plan

ELIPSE : Livre sur Fargas, Zorgoth lame de braise, réveiller des Items

livre fargas m'apprend que la flame = Zorgoth

Différence entre mon papa et ma maman, mon pere est un MONSIEUR et ma mere est à la KOUISINE
Ma soeur est plus comme papa 
Camilla débarque et elle passe un peu de temps avec nous
Elle est sympa lol
Lueur est curieux et il est très empathique 
Caleb reste assez froid avec moi et c'est réciproque
Caleb et moi on fait une petite trève
On se met en route pour Port Ecarlate 
Et on s'arrete à Blanchecolline chez Mimi et Joseph
Les Greenemeraude ont cramé une grange à Blanchecolline 
Un goliath 
On va prendre un bateau pour aller à Aqar 
Le bateau c'est le foudroyant et le capiatine c'est Kar 
On croise Orcan ce sacré BG
On prend le bateau 
On s'arrete à Port Daran et on va au cirque
On passe pas mal de temps dans la boutique d'Estalar Hadjar
Je prete 500po à Donaar 
Un objet magique endormi s'upgrade avec moi à force de l'utiliser 

On s'approche des ruines et je reste persuadé que ya pas de danger 
On fumes les gnolls et les hyènes et ca a failli mal se passer

### Akar et les orcs gentils

On passe à la boutique pour les champi
On achète de quoi picoler 
On va se promener en cheval avec Caleb Galan et Nawelis cété rigolo
On arrive en Dramacna et on se fait attaqué par du vent qui en voulait aux pieces de golems 
On marche jusqu'a Akar

On va voir l'ancient boss de Nawewe
Je sais pas ce qu'il se dit tho
On dort, le lendemain je vais avec Galan et Nawewe voir son chasseur de tête
Ya le temple de l'oiseau de feu mais ca me concerne pas
Je suis resté à l'écart ave Galan donc on a entendu R
On achete des carters et j'achète un panier pour Lueur (15po, 2 jours)

Je vais me promener au bord du lac et je vais regarder les bateaux 
Je cast plusieurs sorts dans le lac 
je vais au hall des pecheurs pour acheter du poisson
On part avec Braxxar vers un endroit pour Nawelis

On tombe sur des orcs gentils dans une foret magique 
Ya la mere de Nawewe qui vie avec les orcs

Ya des orcs méchants qui sont pas gentils avec les orcs gentils 
Nawelis décide que les orcs sont tous cons et qque c'est des connards (ca peut se comprendre)
On se casse sans regarder derière nous
On décide d'aller buter l'orc méchant finallement parceque Caleb pense pouvoir fabrquer son épée 
On voyage vers chez les orcs dans la montagne 

Ya trop de goblin qui m'aiment 
On rencontre Baldo et Sarya qui sont super chouettes
On détruit tous les goblins j'en crames une vaste majorité avec Galan 
Ya cinq mlilles orcs dans la montagnes, on décide de partir chercher des renforts

***

On décide de prévenir Akar de la présence des orcs
En fait on va prévenir le giga mage et aller à Ambrecote car c'est dans la juridiction de l'empire 
On arrive à la ferme de Bren (on avait défendu sa ferme au début) contre des voleurs 
Tous les environs ont été ravagés par des gnolls et "ya eu aucun survivants" 
On leur explique que ya des orcs dans la montagne 
On trace vers Griffinvale
On arrive à Griffinvale et Althea la **pute** nous écoute mais c'est une **pute**
L'empreur arrive et il nous écoute vraiment lui 
On dort à l'hotel

***

### Préparation à la guerre 

Le lendemain on va faire les boutiques
On croise un vendeur de potion qui s'appel Kqvar
Je demande aux darons de Saphir si ils connaissent quelque chose sur les phoenix 
=> ils y connaissent r mais c'est pas étonnant 

Le lendemain je suis parti me promener

***

Je vois une bestiole chelou : tete d'oiseau 4 pattes et corps de félin  
D'après Galan soit disant c'est un griffon 
On va au giga conseil de guerre 

Le général c'est Avoril marchetemps

> Retrait des villes de l'empire

Commande de donnaar "Au pres de aubrakabestant boutique du vecteur sur le port au nom de Donnaar"

On passe par port Daran 
On va faire coucou à la boutique d'Estalar
Ca fait mal au cul o m g (j'ai donné 1500 po pour R)

***

Je récupère la commande de Donnaar
a priori, on sera tranquil pour faire ce qu'on veut dans a montagne mais faudra quand meme voir avec le capitaine

On rencontre la capitaine Okira Kulmadas

Le soir on va dans une salle de jeux avec Caleb, Galan, Nawewe et Baldo
On mise 50po en jetons 
j'ai fais une soirée insane mais j'ai tout perdu à la fin à cause de l'alcool et de la fievre du jeu je suis un peu dègue parcequ'on a chatouillé plusieurs milliers de po

> 50 jetons -> 85 jetons (+ 40 jetons de nawewe) que j'ai tout perdu

Je suis absolument dépité de cette soirée horriblement affreuse 

on prend la route avec la tete dans le giga cul
On retourne à la montagne. 

***

### Bastion orcs

On se fait attaquer par des monstruosités monstrueuse
J'ai fini dans le bide d'une de ces merdes mais on les a tué quand même 

On fait un pacte avec les kobolds pour qu'ils nous laissent passer 
On fait un deuxième pacte pour qu'ils nous filent la clef  

***

On se bat contre des chauve souris géantes (c'est elles qui bouffent la kobold a priori) et on gagne parce qu'on est trop forts 

On décide de pas aller plus loin (même si dans l'idée on laisse un peu le travail à moitié fait)

On passe une enigme stylé avec les deux gardiens pour choisir une porte qui nous emène dans le bastion orc 

On fait s'échapper 4 prisonniers des orcs qui nous donnent quelques informations (ya Draz le goblours cool)

***

On lache baldo et zarya qui vont s'occuper des esclaves et on garde Draz pour nous guider
On est vraiment des as de la discrétion (enfin presque)

***

On dort. ZZZZzzzzZZZzzzZZzZZZzzZzZ
Caleb fait une jolie connerie rapport à un rat 

Galan explique en quoi consiste le pacte qu'il a fait : 

```PACTE
Mon ame devient lié au démon 
Au moment de ma mort le démon peut en faire ce qu'il veut 
En échange je gagne des pouvoir 
```

Draz fait un pacte avec le démon de Galan pour devenir plus fort dans l'espoir de sauver ses potes 

Le plan c'est d'attirer Grogar pour essayer de le retourner contre Cronac (envoyé Grumsh) 

***

> Ecoutez moi braves orc, écoutez l'envoyé de grumsh : vos légions sont forte et je sens le courage couler dans vos veines 
>
> Malheureusement je sens aussi la coruption dans vos rangs : Cronac vous trompe il ne suis pas la parole de Grumsh
>
> Votre vrai chef ne devrai pas s'appeler Cronac, votre vrai chef se trouve ici : amener moi Grogar dites lui que l'envoyé de Grumsh le demande ! 

Je fais un joli discours pour attirer l'attention des orcs au niveau de la porte des forges. Les orcs me prennent pour un dieu je suis trop heureux ca marche trop bien.

Grogar c'est le plus con des clients ca a trop bien marché. Au passage je suis le ***Kromosh***.



On va dans les grottes pour essayer de liberer plus d'esclaves
On libère les esclaves des mines 

***

On discute de si on part ou pas. Je part avec Nawelis pour soit des bateaux, soit des caravanes on sait pas trop mais on va bien rigoler.

Je claque la bise à Galan parceque c'est mon pref <3

## Darlin

### Dans le bastion

On rencontre les ombres éclatantes (j'en avait entendu parler, groupe chaotique qui détruit plutot que réparer allié avec des voleurs locaux... Un groupe plutot bof)

On arrive avec [Baldo et Sarya](./Wiki/PNJ.md) d'allieurs

On se réfugie dans un entrepot, je répars la porte de l'entrepot pour qu'elle soit niquel. 

Je discute pas mal avec Caleb puisque lui aussi il suit un dieu, [Taranys](./Wiki/Pantheon.md). 

Caleb me laisse toucher son épée et c'est une très belle épée

La porte vole en éclat et la musique de fight se lance

***

On se bat contre des orcs et chaman et ogres sa mère mais on gagne parce qu'on est trop fort 

On va se reposer derrière la porte magique 

***

Les orcs sont partis et on décide d'aller dans la salle d'archive pour Saphir.

Il reste des sentinelles, tous les orcs sont pas partis non plus

On se fait attaquer pendant qu'on est dans les archives
Kronac a sauté et on va lui péter la gueule.

***

On se bat contre Cronak mais il arrive à s'enfuire

> Pv max : 80
> Pv init : 80
> -23 => Il te reste 57pv
> Je suis frappé par la foudre au plafond
> -2 => Il te reste 55pv
> -8 => Il te reste 47pv
> Je suis sous esprit confus
> Cronak se bat contre Caleb et je vais essayer de le sauver
> Je heal Caleb et j'essaye de taper Cronak mais je rate
> Cronak lance un spell bisarre necrotique mais j'arrive à résister (Caleb bof par contre)
> -10 => Il te reste 37pv
> -8 => Il te reste 29pv

(Prochaine session je lance prière de gérison lv4)

***

### Après le bastion

On a une petite discussion avec Caleb et il s'avère que Galan "aurait" fait un tout petit minuscule PACTE AVEC UN DEMON. Il faudra que j'ai une petite discussion avec le bonhome

On se tp à Griffinvale

Galan me donne des objets à identifier :
Sachet de cristaux d'arcanite
Livret de vivacité
Sac de couchage stylé

Leo nous explique son histoire, son papi est parti en voyage et est renvenu puis lui a donné un anneau et depuis leo fait de la magie.

On rencontre Joda l'elfe de l'armée de Grifinvale (l'armée du Griffon)
[Joda](./Wiki/PNJ.md) est grave cool et il nous donne une "faveur"

Je forge une armure pour loutre pour Leopold.

On se TP à port Daran

Je reste à Port Daran avec Galan car il a mission à remplir pour récupérer quelque chose de la part d'Estalar. Mon but est de discuter vite fait avec le bonhomme

***

On récupère l'oeil de liche pour Galan auprès d'une meuf sous champi ultra chelou.

Le lendemain le démon de Galan (Kirsal) nous parle à tous. "J'y suis pour quelque chose"
Du coup tout le monde embrouille Galan

Il nous explique qu'il a fait un contrat avec un diable pour pouvoir arriver à ses fins. 

> Galan veut se venger de sa famille [Siannodel](./Wiki/Siannodel.md) : qui font du trafic d'être humains. Il s'est enfuit en étant ado à Cornécaille. ya un an il a fait son pacte à un moment de faiblesse. 
> heureusemebnt Elyo a réussi à aranger le contrat parce qu'il est cool. Il a fait le contrat en connaissance de cause
>
> Il est brisé. On a une vision des choses trop différentes

Je vais prendre le sort scrutation pour essayer de retrouver les soeurs de Galan.

Je lance sructation sur Lagae mais ca ne marche pas

J'améliore l'armure de Galan 

***

La maintenant le but c'est de : aller vers Norska (pour Donaar, Saphir)

### En route vers Norska

On part vers Arcvale en bateau. 

> Pour aller sur le toit du monde (Saphir), pour trouver les grisdemeraude qui ont surement une pièce de golem (Donnaar), ya une organisation de fdp à Ventreruine qui est dans le nord. Il font de la drogue pour le réseau des Syanodel (Galan). Leopold à un demi-oncle qui serait à Givrefort.

Sur le bateau j'arrive à contacter Lajae la soeur de Galan, elle est avec sa soeur. Elles savent pas ou elles sont et elles sont retenus prisonnières.

Arrivé à Arcvale, la famille Titarion nous accueille et offre des cadeaux à tous (sauf moi et leopold)

Koxvar a aussi un cadeau et une lettre qu'il ne récupérera surrement jamais.

La famille des Titarion nous propose de récupérer le temple du feu et de le réhabiliter pour nous. On accepte.

J'utilise 3 jours pour améliroer l'armure de Caleb de Donaar et de Moi. Et ca c'est chouette.

On participe à la fete de la victoire de bahamut

Je cause un peu avec Donaar qui m'explique qu'il cherche à renouer avec sa foie en Bahamut. Je lui donne mon soutient et je lui dit qu'il est cool.

Je setup le mot de retour dans le temple du grand marteaux à Arcvale

Pour améliorer l'amure de Caleb, Galan et Donnar : 

 * Tharmekhul (demi-dieu de la forge et du fourneau)
 * Haela Hachebrillante (Demi-déesse de la battaille et de la chance)
 * Clangdinn Brabargent (Dieu des batailles, de l'honneur)
 * ET finallement Moradin (dieu des nains et père de toute choses, juge dur mais juste)

***

[Galan](#Les-copains) a retrouver le [dossier](./Annexes/dossier_des_De_ignas.pdf) sur sa famille les *De Ignas*

> C'est impossible d'aller direct à Ombrelune, c'est trop protégé.

Donaar se vénère en lisant le dossier :

> Donaar s'est fait attaquer son village et [Zatur](./Wiki/PNJ.md) etait le chef de l'attaque, Donaar l'avait laissé pour mort mais manifestement il est pas mort.

[Caleb](#Les-copains) me demande quelque chose :

Il veut que j'incorpore ses lunettes de vision nocturne à son casque (à peu près une demi-journée de travail)

Je m'occupe de son casque pendant que les autres lisent et décryptent le dossier des [De Ignas](./Wiki/DeIgnas.md).

On comrpend que les soeurs de Galan sont tenues prisonnières chez Cabestan, le bijoutier de Port Daran. 

On décide de se tp là bas pour faire une intervension. 

On rentre dans la boutique 

Il a effectivement les gamines, il a une lettre signé du *voile d'obstidienne*. La lettre lui dit de garder les gamines et que quelqu'un viendra les chercehr une jour.

On récupère les deux soeurs, on se tp à Griffinval.
On demande au premier ministre de l'empire de s'occuper des deux filles (il nous devait un service)

On place les deux filles dans une planque bien sécu.

Quand les autres vont picoler, je vais dans le temple de Moradin pour aider le maitre forgeron de Griffinvale. 

***

La on va à Norska : pour aller sur le toit du monde (Saphir), pour trouver [les gris d'émeraude](./Wiki/PNJ.md) qui ont surement une pièce de golem (Donnaar), ya une organisation de fdp à Ventreruine qui est dans le nord. Il font de la drogue pour le réseau des [Siannodel](./Wiki/Siannodel.md) (Galan). Leopold a un demi-oncle qui serait à [Givrefort](./Wiki/Givrefort.md).

On marche jusqu'au fleuve à l'est de [Griffinvale](./Wiki/Griffinvale.md), jusqu'au quai du vieu Connor.

Donaar entend deux mec jeter une malle dans l'eau. Dans cette malle, il s'avere qu'il y avait plein de morceau de gens. On arrive pas à retrouver les deux mec donc on abandonne les recherches.

On embarque sur des barques pour espérer trouver un plus gros bateaux au quai du vieu Bromir.

J'aimerai faire une petite effigie de [Bahamut](./Wiki/Pantheon.md) en métal pour [Donaar](#Les-copains), Saphir m'a donné une dague pour ca

On remonte le cours d'eau jusqu'au quai du dessus. 
On raconte des histoires à un barde (histoires  des ombres éclatantes et aussi l'histoire des 3 héros et la montagne)

Je donne la petite statuette de [Bahamut](./Wiki/Pantheon) à [Donaar](#Les-copains)

La caravelle arrive, on va pouvoir remonter le cours d'eau

Le capitaine s'appelle Hubert de la vallée

On arrive à [Givrefort](./Wiki/Givrefort.md), c'est clairement une ville militaire

***

[Donaar](#Les-copains) retrouve ses potes
[Orin](./Wiki/PNJ.md), Gaston, Gale, et la capitaine Tara

On fait la fête

[Leopold](#Les-copains) retrouve son Oncle
Son Oncle avait un moule de pièce de Golem qu'on a récupéré

> je réflechi à un système pour faire un systeme pour enfiler plus rapidement une armure lourde (il me faut une semaine de travail pour une armure) 

On part en direction de [Norska](./Wiki/Norska.md) dans la vallée des Echos

On croise un homme dans la neige qui répète en boucle "[Thion Urufar](./Wiki/Pantheon.md)" ce qui est bisarre vu que c'est mon nom irl

Le mec s'évanouie, [Caleb](#Les-copains) le fait revenir

Thion Urufar serait le "champion de [Taranis](./Wiki/Pantheon.md)" il seraiat enfermé quelque part et dans la boite y'aurait la clef de sa prison

### Norska

On repart le lendemain ya des secours qui sont venus chercher le mec pour pas qu'il meurt de froid

Ya des nomades qui nous attaquent

> On se fait attaquer par des nommades alors qu'on marche vers l'étoile du nord
> Je fais apparaitre mon gardien de la foi pour faire écran devant Gell et Galan
> Ils courent autour de nous en cercle et nous criblent de fleches
> Je fonce sur un des traineaux pour essayer de les ralentir je bousille le traineau et il prend feu
> Leopold rajoute un éclaire sur le traineau que je viens de toucher
> Sa louttre transformée en dragon vient en plus en renfort et mange un des mecs sur le traineau
> Tous les traineaux finissent par s'enfuir on a gagné

Askip les nomades sontn paas sensé nnous attaquer dans cette vallée, les soldats avaient des accords qui ont étés violés

Moradin me confirme que rester avec les Ombres Eclatantes me permettra de continuer ma quête.

le lendemain on se fait attaquer par un géant 

> Le géant lache un gros balayage sa race
> je lui met un gros coup de marteau et de marteau spirituel
> Le géant prend Leo transformé en singe géant et nous l'abat sur la gueule
> Caleb double smite le géant on a gagné

On arrive un avant-poste qui se trouve sur le chemin de l'[étoile du nord](./Wiki/Norska.md)

***

On passe la nuit dans cet avant-poste.

Le soir suivannt, on trouve un veilliard [Hulfar](./Wiki/PNJ.md) qui fait des aller-retours enntre [Givrefort](./Wiki/Givrefort.md) et les avant postes. 
On l'invite à passer la nuit avec nous.

On tombe sur un poteaau entouré de cadavre de chiens de traineaux attachés audit poteau.

[Hulfar](./Wiki/PNJ.md) le fdp nous a volé pendant la nuit

On arrive à l'avant poste le lendemain mais juste avant d'arriver, l'avant poste se fait attaqué par un genre de rammus géant de glace

> Le fort se fait attaqué par un Ours-hiboux de glace mais on intervient et on lui pète la gueule

Je recontre dans le camp [Camilla Brillant](./Wiki/PNJ.md) qui est une ancienne pote des Ombres Eclatantes

On doit passer chercher le rodeur [Godol](./Wiki/PNJ.md) qui nous guidera dans [Norska](./Wiki/Norska.md). 

Les [Gris d'emeraudes](./Wiki/PNJ.md) sont passés par le camp et sont paartis il y a 8 jours.

La nuit [Caleb](#Les-copains) pete un cable parce que dans la nuit il a vu [Kirsal](#Les-copains).
On comprend que [Kirsal](#Les-copains) s'est foutu de la gueule de notre drakeide préféré. On dessend pour discuter et au cours de la discusion [Kirsal](#Les-copains) accepte de venir discuter.

***

[Kirsal](#Les-copains) nous emmène dans son chez-lui pour discuter . Il est d'accord pour arreter de nous parler (il en fait la promesse). Manifestement j'avais vu juste, [Kirsal](#Les-copains) gagne en puissance grace aux actions de [Galan](#Les-copains), c'est chiant. J'apprends aussi que pour rompre le contrat, il faut l'accord des deux partis (peu importe qui veut en premier).

Galan me raconte le moment ou son frere lui a tranché la gorge.

Communion : 

> Eset ce que je peux aider Galan à rompre le pacte ? - Oui
>
> Fazut il craindre Kirsal ? - Moradin sait pas 
>
> Est ce que Galan est toujorus maitre de lui ? - Oui

Le lendemain on part je raaconte à [Galan](#Les-copains) et [Caleb](#Les-copains) ce que [Moradin](./Wiki/Pantheon.md) m'a dit. 
On marche un jour sans soucis.

On dort, pas de galères.

Aujourd'hui il neige fort : tempète de neige sa race
On arrive à s'abriter dans une caverne de Kobolds  

On repart le lendemain, il fait beau

***

On combat les gnolls et ours-hibou

> Pv max : 88
> Pv init : 88
> Les gnolls etaient en train de chasser le ourse-hibou 
> Ya un gnoll qui attaque le ourse-hibou
> Saphir et Donnaar se sont tp en haut
> -10 => Il te reste 78pv
> Un gnoll m'attaque
> En fait le ours-hibou c'était un mec transformé et les gnoll le font tomber inconscient
> Je fonce sur les gnolls qui sont en train de manger le mec
> Je défonce un gnoll, le fait reculer et je me mets entre les gnolls et le mec
> Leo fait apparaitre sa loutre-dragon légèrement op pour gérer les gnolls de derrière
> Caleb essaye de forcer le passage mais n'arrive pas à passer 
> Il se retrouve juste à coté de oim pour protéger jean-mich
> Ya un gnoll qui me passe par dessus pour focus jeanmich
> J'utilise faconnage de la pierre pour faire un oeuf de protection autour de jeanmich
> j'invoque un gardien de la foi pour bousiller tous les gnolls qui passent dans le coin
> Je me fait attaquer par 2 gnolls je prends des dmgs
> -11 => Il te reste 67pv
> -14 => Il te reste 53pv
> 2 gnolls m'attaquent
> -4 => Il te reste 49pv
> -23 => Il te reste 26pv
> -5 => Il te reste 21pv
> +15 mot de guérison => Il te reste 36pv
> Donnaar et Saphir ont tué le chef et ducoup les gnolls s'enfuient

J'envoie un message à [Camilla brillant](./Wiki/PNJ.md) pour la prévenir du retard qu'on a pris.

Le traqueur qu'on a suavé s'appel [Godol](./Wiki/PNJ.md). C'est lui qu'on cherchait
Il nous emmène à sa cabane pour qu'on puisse discuter
Sa cabane est pas incroyable mais il est cool

ASkip les [Grisdemeraude](./Wiki/PNJ.md) voulait aller à la main de dieux. On va essayer de les suivre là bas

On s'installe pour passer la nuit chez [Godol](./Wiki/PNJ.md)

Le lendemain [Godol](./Wiki/PNJ.md) va mieux et on va partir 

On se prend une giga tempete de glace dans la gueule (à arracher les tuiles) Ya des blocs de glaces qui tombent carrément du ciel.

Le ledmain j'ai archi mal dormi et je suis turbo fatigué. J'essaye de me reposer le maximum. [Galan](#Les-copains) me prête son sac de couchage de comfore pour que je puisse mieux récupérer (merci)

On arrive dans [la faille des ames](./Wiki/Norska.md) faille profonde dans le sol ou ya des ames qui errent, il faut pas faire de bruit sinon les ames risque de nous entendre

***

On avance dans la [faille](./Wiki/Norska.md), ya des spectres horribles qui passe juste à coté de nous on a tous des allus : 

> Je vois des orcs
> Je perds mes moyens 
> Les orcs me terrifient => j'ai honte d'avoir peu d'orcs alors que ca devrait pas

[Donaar](#Les-copains) m'attrape et me fais revenir à moi (merci mon reuf)

[Donaar](#Les-copains) a une vision aussi

J'essaye de le choper mais je rate comme un caca nanique
Il se fait traverser par un spectre et tombe inconscient

[Léopold](#Les-copains) a une bonne idée et décide de transformer le corps inconscient de [Donaar](#Les-copains) en araignée pour qu'on puisse le transporter facilement

On arrive au moment ou il faut traverser la [faille](./Wiki/Norska.md), je décide de sauter avec aavec Donaraignée maais le bruit que je fais réveille les spectres. Ils se jettent sur nous.

[Galan](#Les-copains) arrive à voler au dessus du gouffre maais tombe inconscient  à cause d'un spectre

Je chope mon médaillon de [Moradin](./Wiki/Pantheon.md) et je lance renvoie des morts viviants et on arrive à courrir loin de la [faille](./Wiki/Norska.md)

J'arrive à parler avec [Donaar](#Les-copains) du fais que j'ai eu peur d'orcs pendant mes visions

[Donaar](#Les-copains) me réconforte en disant que c'est normal d'avoir peur, il est super cool

On passe la nuit
Pendant mon tour de gaarde je vois un point de lumière dans le lointain. Mais [Saphir](#Les-copains) les voit pas.

On croise des nains daans les montagnes qui acceptent de nous laisser passer. Ce sont des nains du [roi Kilgar](./Wiki/Norska.md)

On passe la nuit

Le lendemain [Godol](./Wiki/PNJ.md) nous montre un volcan au milieu de [Norska](./Wiki/Norska.md)
Ya un genre de mini ecosysteme acroché sur les bords du volcan et on mange des fruits exotiques 

On passe la nuit

Le lendemain, yaa un gros dragon blanc qui nous fonce dessus

***

> Pv max : 88
> Pv init : 88
> Je lance protection contre une énergie (froid) jsute avant que le dragon arrive
> -21 => Il te reste 67pv
> je jette le marteau du forgeron mais je ratte
> Le dragon me mets un coup d'aile
> -13 => Il te reste 54pv
> Caleb casse l'aile du dragon 
> -7 le dragon nous tombe dessus à Donaar et moi => Il te reste 47pv
> j'essaye d'abattre un arbre sur la gueule du dragon mais je rate
> Je heal Caleb de 11pv
> Le dragon s'acharne sur Donaar
> Le dragon essaye de s'enfuir
> Saphir se tp avec Caleb sur son dos mais les deux tombent
> Leo se tp sur son dos mais il tombe
> Le dragon Chope Donaar et essaye de s'envoler mais je lui pete une aile
> Au sol Donaar le finit en légende

Je réveille [Caleb](#Les-copains) et [Leopold](#Les-copains)

[Saphir](#Les-copains) appelle [Silverbarbe](./Wiki/PNJ.md) pour lui demander de stocker le dragon le temps qu'on se sorte de [Norska](./Wiki/Norska.md). Le prix acté est de une part pour lui soit un 7ième

[Saphir](#Les-copains) exécute un sort hyper couteux avec l'aide de [Silverbarbe](./Wiki/PNJ.md) et elle est en pls. [Silverbarbe](./Wiki/PNJ.md) se casse et le dragon est stocké dans un plan caché.

Je heal bien tout le monde

Je fabrique un traineau pour les blessé le temps qu'on aille à un endroit 

[Caleb](#Les-copains) me file un bouquin sur [Ahkin](./Wiki/Items.md) pour me préparer à la réparer.

J'envoie un message à [Bordar](./Wiki/PNJ.md) pour lui raconter le combat.

[Caleb](#Les-copains) est en full déprime parcequ'il est tombé inconscient pendant le fight

La nuit je vois ENCORE les lueurs de ses morts mais [Donaar](#Les-copains) les voit pas. J'ai envie de crever = je suis dégouté = ca me saoul

On tombe sur une caverne  dabns la caverne ya un piedestal avec une pierre sur le piedestal qui envoie une lumière de ouf

[Galan](#Les-copains) envoie sa main de mage pour prendre la pierre et ils entendent un bruit de léger tremblement de terre

C'est un "crystal de l'aube" qui permet de lancer lumière du jour à volonté. Je me rappel grace à mon éducation de clerc que la pierre :

> La guere  des arcane ou les dieu sont venus aider les mages
>
> Les dieux ont fabriquer des artefactes dont cette pierre. Cette pierre a été fabriquée apr [Pelor](./Wiki/Pantheon.md)

Je file la pierre à [Caleb](#Les-copains)

J'essaye de réparer [Ahkin](./Wiki/Items.md) mais je raté j'ai aps assez de matériel. Demain il faut que j'essaye de fabriquer une vrai enclume et d'autres trucs avec création.

Quand la lueur apparait cette nuit j'ai l'impression de voir une personne. Mais j'ai pas l'impression qu'elle ai de torche, la lumière semble venir directement de lui. Mais [Galan](#Les-copains) la voit pas non plus

Le lendemain on tombe dans un piège tendu par des géants. Ca veut dire qu'on est plus cons que des trucs très cons. C'est très très grave

***

> Pv init : 88
> Un grand filet de grosse corde gelé nous soulève du sol
> -3 du froid des cordes => Il te reste 85pv
> J'utilise protection contre une énergie (froid) au début du combat
> Leo utilise vol sur moir merci le sang
> Donaar prend une claque d'un géant et tombe direct inconscient
> Caleb relève Donaar
> J'ordonne à 3 géants de fuire avec injonction
> Ya une shaman géante qui me lance un javelot de glace qui me gèle les pieds au sol
> Leo balance un éclaire de zinzin sur un géant et la shaman moche
> Les géants comprennent à l'odeur qu'on a tué le dragon
> Du coup ils acceptent de nous escorter à la limite de leur territoire

On monte sur les géants qui nous emènent en courant
On gagne beaucoup de temps grace à eux, merci les gars

On monte le camp avec tout le monde, je m'occupe du bouclier de [Caleb](#Les-copains)

Je fais mon tour de garde avec [Caleb](#Les-copains) mais il voit pas la lueur non plus

[Caleb](#Les-copains) me dit que c'est peut etre [Moradin](./Wiki/Pantheon.md) qui me fait des signes alors je décide de lui poser directement la question

> non c'est pas Moradin qui me fait signe
>
> oui => Il sait ce que c'est 
>
> oui => c'est en rapport direct avec [Moradin](./Wiki/Pantheon.md)

J'identifie la clef de [Caleb](#Les-copains) et c'est proche des pièces des golem de destruction. C'est une clef qui ouvre une cage ou une prison

Le lendeamin on arrive devant une gigantesque arche de glace

On avance et on appercoit la [main de dieu](./Wiki/Norska.md)

![img](https://cdn.discordapp.com/attachments/1170845123035287702/1179539935951331398/Gargantua_-_main_1.jpg)

Je lance détection de la magie à coté de la main : la main est magique (oui) C'est invocation et abjuration.

L'aura de magie de la main semble réagir aussi au sens divin de [Caleb](#Les-copains) et à mon mot de guérison. Et au sort d'ombre chelou de [Galan](#Les-copains). [Leo](#Les-copains) se transforme et la [main](./Wiki/Norska.md) refait pareil

[Galan](#Les-copains) lance un sort ultra chelou qui le recouvre d'ombres

En testant en boucle avec [Galan](#Les-copains), il se passe un truc super bisarre, je souffle des bulles quand j'essaye de parler (pendant une minute). [Galan](#Les-copains) recoit un truc mais 

On arrive au point [centre archéologique](./Wiki/Norska.md). On rejoins un gros trou dans un gros truc de métal. Ya un campement qui date de la veille (ou de 2 jours grand max). C'est le buste d'un golem de destruction

***

### Dans le Golem de destruction

On commence à descendre dans la grotte/golem dans le but de chercher les [Gris d'Emeraudes](./Wiki/PNJ.md)

On tombe sur un plan du golem (qui nous donne une carte de la grotte)

![img](https://cdn.discordapp.com/attachments/532638660315840522/1181340501836832808/Carte_du_Titan_des_Arcanes_-_Norska_Plan_de_travail_1.png)

On trouve des batons magique dans le golem  qui permettent de déplacer des caisses

![img](https://cdn.discordapp.com/attachments/532638660315840522/1181296037063835699/Baton_de_controle.png)

> Pv max : 96
> Pv init : 96
> On rentre dans une pièce, on se retrouve tp dans différentes pièces
> Je me retrouve tout seul 
> Ya un robot dans la pièces
> -50 rayon electrique => Il te reste 46pv
> Ya un deuxième robot
> -15 le robot m'enfonce dans le mur => Il te reste 31pv
> +12 mot de guérion => Il te reste 43pv
> J'ouvre la porte car j'entends Caleb de l'autre coté et je le rejoins
> Nouvelle vague de magie je retourne dans la meme pièce avec Caleb
> Galan ouvre la porte derrière moi
> Un golem me fonce de ssus et m'attrape
> -18 il me mets une claque => Il te reste 25pv
> Caleb arrive à faire très mal au robot en tappant dans leur coeur
> J'essaye de faire pareil mais je rate
> +8 mot de guérison => Il te reste 33pv
> Je me refais tp je suis tout seul encore
> Un golem arrive et me chope
> -10 => Il te reste 23pv
> +8 mot de guérison => Il te reste 31pv
> Je me refais tp dans un nouvel endroit
> +12 soin de groupe => Il te reste 43pv
> Je relève Galan qui était tombé avec le soin de groupe
> Re tp : je suis solo... encore...
> Les robots se désactivent (grace à Saphir askip)

On se repose

On essaye d'aracher un des coeurs de golem avec [Leo](#Les-copains). Le coeur nous explose à la gueule

On continue à avancer dans le golem

Au moment ou Saphir attrape 2 orbes et ya une voix aigue qui dit "NAN C'EST A MOI CA ! "

***

C'est un gnome des profondeur. Il s'appelle [Hector SansLeHache](./Wiki/PNJ.md) scout de la companie des serpent et commandant de la deuxième cavalerie d'Axamar. Il vient de l'outreterre (loin sous la terre) . Il est arrivé ici pour essayer de récupérer des badges de scout.

On décide qu'on est pressé et qu'on a pas le temps de discuter avec le guignol

Je lance un sort de détection des pièges et je prend un sursaut magique : invertion de gravité

> [Caleb](#Les-copains) marche sur une araignée et y'en a 2 qui sortent de sous un tas de neige
> [Caleb](#Les-copains) se retrouve paralysé à cause d'une araignée
> Je tape une araignée avec mon marteau (-15) et je heal [Caleb](#Les-copains) (+7)
> -16 une araignée me tape => Il te reste 58pv
> Elle me paralyse pendant une minute
> -27 de froid grace à sa paralysie => Il te reste 31pv
> [Caleb](#Les-copains) de tape avec sa main et enlève la paralysie merci mon reuf
> -6 je retombe au sol => Il te reste 25pv
> +11 je me heal => Il te reste 36pv
> Galan reprend un sursaut magique et perd tous ses vetements
> +9 Je me heal => Il te reste 45pv
> Je découvre que [Galan](#Les-copains) est à poil
> +7 [Caleb](#Les-copains) me heal merci mon reuf => Il te reste 52pv
>
> Galan transforme tous ses gold en pièces de bronze à cause d'un autre sursaut magique
> Je tape le dernier coffre et je le tue

On trouve des batons de controle des cubes magiques. Maintenant tout le monde en a un

On continue d'explorer le golem. On tombe sur un gardien arcanique (ta mère). En fait ya 2 gradien arcaniques qui sont désactivés. On avance.

***
On rentre dans une salle complètement replie d'araignée moche de glace du cul. C'est ABOMINABLE

> On se split dans la salle 
> ya plein d'araignée dans la salle on se fait attaquer
> Je fonce pour aider [Caleb](#Les-copains) mais je finis gelé dans une toile
> Ya des araignées qui m'attaquent
> -10  => Il te reste 60pv
> -14 => Il te reste 46pv
> [Caleb](#Les-copains) coure pour me rejoindre mais tombe inconscient devant moi
> Je me retrouve encerclé d'araignées avec [Caleb](#Les-copains) inconscient devant moi
> Je crée un mur de feu autour de [Caleb](#Les-copains), [Donaar](#Les-copains) et moi 
> La grosse araignée fait le tour du mur et gèle [Donaar](#Les-copains) et [Saphir](#Les-copains) (qui était invisible askip)
> Je heal [Caleb](#Les-copains) mais je trigger le sursaut magique et ya des tentacules magiques qui sortent du sol et qui se mettent à me cahtouiller
> -14 en passant mon mur de feu => Il te reste 32pv
> Je pars essayer d'aider [Galan](#Les-copains)
> Je me fais gelé par une toile (encore)
> Toutes les arraignée s'enfuient
>
> On arrive à buter la grosse araignée qui était coincé par un mur de force de Saphir

On passe vite à la salle suivante ou ya un camp

On envoie Leopold transformé en mouche pour explorer

[Leopold](#Les-copains) explore la salle (cul de sac) avec le camp, elle est vide

***

On décide de continuer plus profond dans le Golem sans s'attarder sur le camp. On continue vers la tête (qui est après le pied droit).

On rentre dans une salle avec des cuves vides. Ya des flaques vertes au sol avec des truc chelou dedans mais on voit pas bien.

> Ya [Leopold](#Les-copains) qui gueule mais je sais pas bien pourquoi. 
>
> Ya [Caleb](#Les-copains) qui se prend une créature chelou blanche sur la tête. 
>
> On arrive à se débarrasser des bestioles. 

On fonce vers la porte du fond. Elle est fermée. J'essaye de mettre un coup de marteau, ca marche pas. Après analyse de la porte, il faut une clef spéciale. On fouille la salle et [Leo](#Les-copains) trouve le morceau qui sert de clef.

Dans la salle d'après on doit traverser un gouffre. [Leo](#Les-copains) transformé en chouette géante nous fait traverser un par un. Alors que j'attends en dernier. Je vois la silouette que j'avais vu plusieurs fois dans les montagnes. En plus clair: j'ai l'impression que c'est quelqu'un mais impossible de vraiment distinguer.

A travert des pans de roches, on arrive à distinguer la tête du golem. Sur notre droite, à travert un gros morceau de glace transparent, on voit les gris d'émeraudes de PUTE.

***

> -5 de feu a cause d'un spell => Il te reste 52pv
> Le clerc lance un sort (elle a un symbole de Tiamat)
> -7 de force (meme spell) => Il te reste 45pv
> La magicienne fait apparaitre un mur de force devant nous mais Leo arrive à la déconcentré et le mur disparait
> En deplaçant les cubes au dessus du vide, on arrive à passer
> Leopold Lance une chaine d'éclaires à travers la glace et bousille la magicienne
> Je le vois pas mais la clerc soigne la magicienne
> j'entends que Galan est en galère, je cours vers lui et je lui rends 70pv
> Je prends un carreau d'un mec invisible
> J'arrive à relever Saphir qui était tombée inconsciente
> Les mec invisibles sont toujorus dans le coin

***

> Caleb Donaar et Leo se battent toujours dans leur coin on est laissé à nous meme avec Saphir
> Je gueule à Saphir de se cacher derrière moi, on va essayer de gagner du temps
>
> Par contre j'essaye d'appeler Moradin et meme si ca rate je revoie encore la silouhette dorée. Je suis maintenant  sûr que c'est lié à mon dieu
>
> Le clerc de Tiamat fait un retour et ils se cassent tous avec la pièce de golem

J'appel Galan, il va bien

On va se reposer dans la tete du golem

En allant prier la lueur revient et s'approche de moi. Elle se transforme en nain spectral et me parle.

Je m'appelle Trahin Coeur de Feu
C'etait un devoué à Moradin pendant les battailles entres les dieux et les hommes et les autres dieux. Il s'est cristalisé en Norska

Tant que je serai en Norskar il sera à mes coté. Il me tend un amulette que je prends l'amulette. Elle permet de réchauffer autour de moi si j'en ai besoin. Il me dit que je suis super cool et c'est vrai.

J'allume une bougie car il est supoer cool aussi

Je suis super content et je vais m'occuper de mon armure. J'explique à tout la rencontre que je viens de faire. Donaar me donne des objets à analyser.

![img](https://cdn.discordapp.com/attachments/532638660315840522/1204885076395954216/bandeau_intelligence.png?ex=65d65bab&is=65c3e6ab&hm=cb26194cfdfe33db36b3bbf72ad03548daae70bb5f4e1b56d21d268174832398&=)

![img](https://cdn.discordapp.com/attachments/532638660315840522/1204885113062686781/Fouet_de_lesclavagiste.png?ex=65d65bb4&is=65c3e6b4&hm=4eb44e7d056fe39ac251633cee550918c78ec8e658fb774d943fd5730c4951dd&=)

![img](https://cdn.discordapp.com/attachments/532638660315840522/1204885200245624862/Morceau_de_Titan.png?ex=65d65bc9&is=65c3e6c9&hm=e6ae68da393aad1accfd84ae9c3836b82ad1f5847ceefb38b168b8dff5b46c81&=)

![img](https://cdn.discordapp.com/attachments/532638660315840522/1204885379258253373/Hurlegivre_-_endormi.png?ex=65d65bf4&is=65c3e6f4&hm=0df0bee1716e58b40d4bf330f8034d9c1547251f449833f1c332e3f78a488e06&=)

On dort très bien

On discute et on décide d'explorer le reste du golem avant de repartir

On va visiter la salle avec la lueur bleu
Ya un piège mais j'arrive pas à savoir où exactement

On rentre dans la pièce je passe en premier en disant de bien rester derrière moi du coup Saphir ne reste pas derrière moi et déclenche le piège. La porte derrière nous se ferme et deux bestioles métaliques canines s'activent

> La bestiole me rugit dessus je suis étourdit
> -18 de rugissement => Il te reste 78pv
> Et je lache mes armes
> Donaar passe devant moi pour me protéger
> Je suis stun
> Donaar tape la bestiole
> Je suis stun
> Je suis plus stun
> Je ramasse mes armes je heal la pute (je parle de Saphir bien sûr) et je tape la bestiole
> la bestiole me touche et me fait tomber
> -19 => Il te reste 59pv
> La bestiole me tape au sol mais Donaar me protège avec un bouclier magique
> Je tape la bestiole je fais 21dmg
> -1 Sursaut magique : Je me fais attaquer par une nuée de chauve souris => Il te reste 58pv
> -16 la bestiole me mord => Il te reste 42pv
> Caleb décapite la bestiole bravo

La porte de sortie est toujours fermée, on cherche une solution pour sortir de là.

On trouve le coeur du golem qui est une arme nucléaire. Ya aussi 3 éclats du coeur que Saphir ramasse.

![img](https://cdn.discordapp.com/attachments/532638660315840522/1204926515868082246/Eclat_de_Coeur_du_Titan.png?ex=65d68243&is=65c40d43&hm=db3d2937d0c8b503224cea7457e4a98d8423411b7177b35b806e0a0bafc50ebd&=)

***

On arrive à la salle des souvenir et on rejoue une orbe de mémoire du golem. C'est POV Golem

> C'est une scene de bataille avec pleins de dragons pégase et tout c'est le giga bordel. Ya une créature gigantesque de luimère qui débarque et qui décapite le golem

C'est surement une scene de bataille entre les dieux mauvais et les dieux gentils. (bataille du chaos)

2e orbe : 

> Cette fois ci on a une vu à l'intérieur des terrasse du golem. C'est un abordage du golem par  des elfes méchants. Les mages se battent contre eux. 

On va dans la bibliothèque on trouve des parchemins et deux ou trois autre trucs mais pas dingue

On ressort du Golem. On passe la journée dans le camps de Camilla Brillant. 

### Le toit du monde

Donaar me file un plan pour un enchantement :

> C'est un enchantement qui permet de faire apparaitre une armure ou une arme (action bonus + lien) enchantement 16h de travail diffculté DD18 2000po 

On tombe sur une maison en pierre habitée. Ya une vieille très suspecte mais qui a l'air sympa donc on rentre. La maison est super accueuillante et il y fait chaud. La vieille s'appelle Martine

Elle a préparé des plats pour 6 sans savoir qu'on allait arriver. Elle déplace sa maison de temps en temps et sinon elle attend juste d'avoir de la visite. Martine est une fée, je l'apprends en réparant sa casserolle. Sa maison est une créature qui peut se déplacer. Martine nous propose de nous déposer plus proche du toit du monde à l'aide de sa maison

J'arrive à lancer scrutation sur l'assassin des Gris d'emeraude. Il est pété de thunes manifestement. Il est avec ses collègues. Il sont à cheval en chemin vers. Je vois pas le mercenaire. 

Martine nous fait dormir dans une pièce-forêt ou ya plein de bestioles qui parlent. Leopold arrive aussi à parler avec Lulu.

Martine nous explique que pour réveiller sa maison, il faut un souvenir précieux. Galan dit qu'il veut bien oublier son frère Zebliss. 

En fait c'était un prank et Martine voulait juste voir qui avait des couilles. Du coup grosses couilles Galan. Pendant le trajet je me mets à faire un cadeau pour Galan, du coup devoir : penser à un cadeau.

***

Je fais un bracelet avec une lune en or. Et je prie Moradin en meme temps (je me suis retiré 10po de pièce d'or et 5po d'acier). Je me suis isolé pour faire ca.

Galan suis Martine pour qu'elle lui soigne sa résistance à l'alcool.  Juste avant qu'il se démolisse la gueule je lui donne le bracelet.

Martine chope d'un coup un truc invisible et se transforme en hibou démon. En fait c'était Hector SansLeHache sous une cape d'invisibilité.

![img](https://cdn.discordapp.com/attachments/1170845123035287702/1216451638328426650/martine20true20forme.png?ex=66006fdf&is=65edfadf&hm=b5b792d4af3a1999b3d7dedbd6500bc96e9ae76eb45257e954ab43615d108b7f&=)

En fait Hector nous a suivit parcequ'il nous trouvait cools. Par contre il est super con.

Martine nous dépose en bas du toit du monde. Pour monter, on décide de laisser Leo transformé en gros oiseau nous monter en  faisant des aller-retours.

On fait une pose sur une corniche mais pendant notre tour de garde avec Caleb, on entend un cris de très très gros oiseau.

Le lendemain ya une tempete et du coup on doit attendre dans la hute de Léoumound qu'elle passe.

La nuit on décide d'avancer grace à un Leopold en chouette géante.

C'est assez anecdotique mais on se fait attaquer par 2 griffons.

On arrive en haut du toit du monde. Saphir joue avec une petite statue de dragon

Ya une grosse forteresse qui apparait et on rentre.

Ya encore un mec qui nous attendait. On est au scriptorium. Il a l'air sympa. Il s'appelle <u>Misto Moria Falbalis</u>. Il est ennemi avec l'assemblé du cerbere donc c'est suremnet quelqu'un de cool. Il nous attendait et il s'avait qu'on allait arriver parceque c'est lui qui a filé son bouquin aux darons de Saphir. 

Askip Camilla Brillant elle faisait parti de l'ordre du savoir aussi. Et aussi les parents de Saphir et aussi son frère. On est venu ici parcequ'on ne peut pas se TP ici.

On change de salle et ya un dragon d'argent qui dort ici et en fait c'est un dragon le mec. Le dragon dort et le mec qu'on voit c'est une projection astrale. En gros le dragon protège l'endroit par sa simple présence. 

Erica Ornemage (patrone de la grande archive) c'est aussi une pote à eux. Silvare aussi c'est leur pote. 

Les pièces de Golem : on en a trois et ils ont découvert qu'un mage avait des infos sur les pièces. Il nous montre l'oeil dragonar qui permet de se déplacer dans les plans. Ils pensent que Grimgar de Asmonjar en a une dans un demi-plan à lui. Ils ont envoyé plusieurs équipe d'exploration mais y'en a une qui est pas revenu.

Les Sianodel ont surement des lien avec les elfes noirs d'Axamar aussi. C'est des ancienne communauté d'elfes noirs de l'empire qui vivient maintenant dans une région.

Il nous file aussi des "cadeaux" : cercle de TP pour plusierus villes dans le monde et aussi pour revenir ici. On a accès à la bibliothèque. Il nous donne un manteau d'arcaniste de l'ordre du savoir (il a des poches magiques). Il nous donne un gros caillou qui sert de cercle de TP portatif. 

Leo il sent comme le frère de Mistomoria. En gros un fragment du pouvoir du dragon est arrivé jusqu'à Leopold. 

Je lance scrutation sur l'assassin des Grisdemeraude. Il est dans une salle contrairement à la dernière fois. c'est une salle très lux. Du coup on sait pas si ils sont arrivés à Ombrelune ou si il sont dans une belle auberge.

